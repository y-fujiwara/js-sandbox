// Generated by BUCKLESCRIPT VERSION 4.0.7, PLEASE EDIT WITH CARE
'use strict';

var React = require("react");
var ReasonReact = require("reason-react/src/ReasonReact.js");

((require('./Beetle.css')));

var component = ReasonReact.statelessComponent("Beetle");

function make(position, handleClick, _children) {
  return /* record */[
          /* debugName */component[/* debugName */0],
          /* reactClassInternal */component[/* reactClassInternal */1],
          /* handedOffState */component[/* handedOffState */2],
          /* willReceiveProps */component[/* willReceiveProps */3],
          /* didMount */component[/* didMount */4],
          /* didUpdate */component[/* didUpdate */5],
          /* willUnmount */component[/* willUnmount */6],
          /* willUpdate */component[/* willUpdate */7],
          /* shouldUpdate */component[/* shouldUpdate */8],
          /* render */(function (param) {
              var zIndexStyle = {
                zIndex: String(position[/* idx */2])
              };
              return React.createElement("div", {
                          className: "beetle_wrapper",
                          style: {
                            left: String(position[/* left */0]) + "px",
                            top: String(position[/* top */1]) + "px",
                            zIndex: String(position[/* idx */2])
                          }
                        }, React.createElement("div", {
                              className: "beetle",
                              style: zIndexStyle
                            }, React.createElement("div", {
                                  className: "foot1_3",
                                  style: zIndexStyle,
                                  onClick: handleClick
                                }), React.createElement("div", {
                                  className: "foot1_2",
                                  style: zIndexStyle,
                                  onClick: handleClick
                                }), React.createElement("div", {
                                  className: "foot1_1",
                                  style: zIndexStyle,
                                  onClick: handleClick
                                }), React.createElement("div", {
                                  className: "foot2_1",
                                  style: zIndexStyle,
                                  onClick: handleClick
                                }), React.createElement("div", {
                                  className: "foot2_2",
                                  style: zIndexStyle,
                                  onClick: handleClick
                                }), React.createElement("div", {
                                  className: "foot2_3",
                                  style: zIndexStyle,
                                  onClick: handleClick
                                }), React.createElement("div", {
                                  className: "foot3_1",
                                  style: zIndexStyle,
                                  onClick: handleClick
                                }), React.createElement("div", {
                                  className: "foot3_2",
                                  style: zIndexStyle,
                                  onClick: handleClick
                                }), React.createElement("div", {
                                  className: "foot3_3",
                                  style: zIndexStyle,
                                  onClick: handleClick
                                }), React.createElement("div", {
                                  className: "body_base",
                                  style: zIndexStyle,
                                  onClick: handleClick
                                }), React.createElement("div", {
                                  className: "neck",
                                  style: zIndexStyle,
                                  onClick: handleClick
                                }), React.createElement("div", {
                                  className: "horn",
                                  style: zIndexStyle,
                                  onClick: handleClick
                                }), React.createElement("div", {
                                  className: "horn_bottom",
                                  style: zIndexStyle,
                                  onClick: handleClick
                                }), React.createElement("div", {
                                  className: "horn_top",
                                  style: zIndexStyle,
                                  onClick: handleClick
                                }), React.createElement("div", {
                                  className: "horn2",
                                  style: zIndexStyle,
                                  onClick: handleClick
                                }), React.createElement("div", {
                                  className: "eye",
                                  style: zIndexStyle,
                                  onClick: handleClick
                                }), React.createElement("div", {
                                  className: "head",
                                  style: zIndexStyle,
                                  onClick: handleClick
                                }), React.createElement("div", {
                                  className: "head_bottom",
                                  style: zIndexStyle,
                                  onClick: handleClick
                                }), React.createElement("div", {
                                  className: "head_light",
                                  style: zIndexStyle,
                                  onClick: handleClick
                                }), React.createElement("div", {
                                  className: "horn3",
                                  style: zIndexStyle,
                                  onClick: handleClick
                                }), React.createElement("div", {
                                  className: "horn3_top",
                                  style: zIndexStyle,
                                  onClick: handleClick
                                }), React.createElement("div", {
                                  className: "body",
                                  style: zIndexStyle,
                                  onClick: handleClick
                                }), React.createElement("div", {
                                  className: "body_light",
                                  style: zIndexStyle,
                                  onClick: handleClick
                                }), React.createElement("div", {
                                  className: "body_light2_left",
                                  style: zIndexStyle,
                                  onClick: handleClick
                                })), React.createElement("div", {
                              className: "beetle right",
                              style: zIndexStyle
                            }, React.createElement("div", {
                                  className: "foot1_3",
                                  style: zIndexStyle,
                                  onClick: handleClick
                                }), React.createElement("div", {
                                  className: "foot1_2",
                                  style: zIndexStyle,
                                  onClick: handleClick
                                }), React.createElement("div", {
                                  className: "foot1_1",
                                  style: zIndexStyle,
                                  onClick: handleClick
                                }), React.createElement("div", {
                                  className: "foot2_1",
                                  style: zIndexStyle,
                                  onClick: handleClick
                                }), React.createElement("div", {
                                  className: "foot2_2",
                                  style: zIndexStyle,
                                  onClick: handleClick
                                }), React.createElement("div", {
                                  className: "foot2_3",
                                  style: zIndexStyle,
                                  onClick: handleClick
                                }), React.createElement("div", {
                                  className: "foot3_1",
                                  style: zIndexStyle,
                                  onClick: handleClick
                                }), React.createElement("div", {
                                  className: "foot3_2",
                                  style: zIndexStyle,
                                  onClick: handleClick
                                }), React.createElement("div", {
                                  className: "foot3_3",
                                  style: zIndexStyle,
                                  onClick: handleClick
                                }), React.createElement("div", {
                                  className: "body_base",
                                  style: zIndexStyle,
                                  onClick: handleClick
                                }), React.createElement("div", {
                                  className: "neck",
                                  style: zIndexStyle,
                                  onClick: handleClick
                                }), React.createElement("div", {
                                  className: "horn",
                                  style: zIndexStyle,
                                  onClick: handleClick
                                }), React.createElement("div", {
                                  className: "horn_bottom",
                                  style: zIndexStyle,
                                  onClick: handleClick
                                }), React.createElement("div", {
                                  className: "horn_top",
                                  style: zIndexStyle,
                                  onClick: handleClick
                                }), React.createElement("div", {
                                  className: "horn2",
                                  style: zIndexStyle,
                                  onClick: handleClick
                                }), React.createElement("div", {
                                  className: "eye",
                                  style: zIndexStyle,
                                  onClick: handleClick
                                }), React.createElement("div", {
                                  className: "head",
                                  style: zIndexStyle,
                                  onClick: handleClick
                                }), React.createElement("div", {
                                  className: "head_bottom",
                                  style: zIndexStyle,
                                  onClick: handleClick
                                }), React.createElement("div", {
                                  className: "head_light",
                                  style: zIndexStyle,
                                  onClick: handleClick
                                }), React.createElement("div", {
                                  className: "horn3",
                                  style: zIndexStyle,
                                  onClick: handleClick
                                }), React.createElement("div", {
                                  className: "horn3_top",
                                  style: zIndexStyle,
                                  onClick: handleClick
                                }), React.createElement("div", {
                                  className: "body",
                                  style: zIndexStyle,
                                  onClick: handleClick
                                }), React.createElement("div", {
                                  className: "body_light",
                                  style: zIndexStyle,
                                  onClick: handleClick
                                }), React.createElement("div", {
                                  className: "body_light2_right",
                                  style: zIndexStyle,
                                  onClick: handleClick
                                })));
            }),
          /* initialState */component[/* initialState */10],
          /* retainedProps */component[/* retainedProps */11],
          /* reducer */component[/* reducer */12],
          /* jsElementWrapped */component[/* jsElementWrapped */13]
        ];
}

exports.component = component;
exports.make = make;
/*  Not a pure module */
