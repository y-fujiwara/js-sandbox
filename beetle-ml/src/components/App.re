[%bs.raw {|require('./App.css')|}];

type state = {beetlePoss: list(Beetle.beetleProps)};

type action =
  | Remove(int)
  | Click(int, int);

let component = ReasonReact.reducerComponent("App");

let make = _children => {
  ...component,

  initialState: () => {beetlePoss: []},

  reducer: (action, state) =>
    switch (action) {
    | Click(left, top) =>
      ReasonReact.Update({
        beetlePoss: [
          {
            left: left - 200,
            top: top - 300,
            idx:
              switch (state.beetlePoss) {
              | [] => 0
              | [head, ..._] => head.idx + 1
              },
          },
          ...state.beetlePoss,
        ],
      })
    | Remove(idx) =>
      ReasonReact.Update({
        beetlePoss:
          List.filter(
            (poss: Beetle.beetleProps) => poss.idx != idx,
            state.beetlePoss,
          ),
      })
    },

  render: self =>
    <div
      className="App"
      onClick={
        event =>
          self.send(
            Click(
              ReactEvent.Mouse.pageX(event),
              ReactEvent.Mouse.pageY(event),
            ),
          )
      }>
      <span> {ReasonReact.string("please click anywhere!!")} </span>
      <br />
      <span>
        {ReasonReact.string("The beetle was referred to this ")}
        <a target="_blank" href="https://qox.jp/blog/css-beetle/">
          {ReasonReact.string("site")}
        </a>
        {ReasonReact.string(".Thank you.")}
      </span>
      {
        ReasonReact.array(
          self.state.beetlePoss
          |> List.map((beetleProps: Beetle.beetleProps) =>
               <Beetle
                 key={string_of_int(beetleProps.idx)}
                 position=beetleProps
                 handleClick={
                   e => {
                     ReactEvent.Mouse.stopPropagation(e);
                     self.send(Remove(beetleProps.idx));
                   }
                 }
               />
             )
          |> Array.of_list,
        )
      }
    </div>,
};