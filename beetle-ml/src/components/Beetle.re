[%bs.raw {|require('./Beetle.css')|}];

type beetleProps = {
  left: int,
  top: int,
  idx: int,
};

let component = ReasonReact.statelessComponent("Beetle");

let make = (~position, ~handleClick, _children) => {
  ...component,

  render: _ => {
    let zIndexStyle =
      ReactDOMRe.Style.make(~zIndex=string_of_int(position.idx), ());
    <div
      style={
        ReactDOMRe.Style.make(
          ~left=string_of_int(position.left) ++ "px",
          ~top=string_of_int(position.top) ++ "px",
          ~zIndex=string_of_int(position.idx),
          (),
        )
      }
      className="beetle_wrapper">
      <div style=zIndexStyle className="beetle">
        <div onClick=handleClick style=zIndexStyle className="foot1_3" />
        <div onClick=handleClick style=zIndexStyle className="foot1_2" />
        <div onClick=handleClick style=zIndexStyle className="foot1_1" />
        <div onClick=handleClick style=zIndexStyle className="foot2_1" />
        <div onClick=handleClick style=zIndexStyle className="foot2_2" />
        <div onClick=handleClick style=zIndexStyle className="foot2_3" />
        <div onClick=handleClick style=zIndexStyle className="foot3_1" />
        <div onClick=handleClick style=zIndexStyle className="foot3_2" />
        <div onClick=handleClick style=zIndexStyle className="foot3_3" />
        <div onClick=handleClick style=zIndexStyle className="body_base" />
        <div onClick=handleClick style=zIndexStyle className="neck" />
        <div onClick=handleClick style=zIndexStyle className="horn" />
        <div onClick=handleClick style=zIndexStyle className="horn_bottom" />
        <div onClick=handleClick style=zIndexStyle className="horn_top" />
        <div onClick=handleClick style=zIndexStyle className="horn2" />
        <div onClick=handleClick style=zIndexStyle className="eye" />
        <div onClick=handleClick style=zIndexStyle className="head" />
        <div onClick=handleClick style=zIndexStyle className="head_bottom" />
        <div onClick=handleClick style=zIndexStyle className="head_light" />
        <div onClick=handleClick style=zIndexStyle className="horn3" />
        <div onClick=handleClick style=zIndexStyle className="horn3_top" />
        <div onClick=handleClick style=zIndexStyle className="body" />
        <div onClick=handleClick style=zIndexStyle className="body_light" />
        <div
          onClick=handleClick
          style=zIndexStyle
          className="body_light2_left"
        />
      </div>
      <div style=zIndexStyle className="beetle right">
        <div onClick=handleClick style=zIndexStyle className="foot1_3" />
        <div onClick=handleClick style=zIndexStyle className="foot1_2" />
        <div onClick=handleClick style=zIndexStyle className="foot1_1" />
        <div onClick=handleClick style=zIndexStyle className="foot2_1" />
        <div onClick=handleClick style=zIndexStyle className="foot2_2" />
        <div onClick=handleClick style=zIndexStyle className="foot2_3" />
        <div onClick=handleClick style=zIndexStyle className="foot3_1" />
        <div onClick=handleClick style=zIndexStyle className="foot3_2" />
        <div onClick=handleClick style=zIndexStyle className="foot3_3" />
        <div onClick=handleClick style=zIndexStyle className="body_base" />
        <div onClick=handleClick style=zIndexStyle className="neck" />
        <div onClick=handleClick style=zIndexStyle className="horn" />
        <div onClick=handleClick style=zIndexStyle className="horn_bottom" />
        <div onClick=handleClick style=zIndexStyle className="horn_top" />
        <div onClick=handleClick style=zIndexStyle className="horn2" />
        <div onClick=handleClick style=zIndexStyle className="eye" />
        <div onClick=handleClick style=zIndexStyle className="head" />
        <div onClick=handleClick style=zIndexStyle className="head_bottom" />
        <div onClick=handleClick style=zIndexStyle className="head_light" />
        <div onClick=handleClick style=zIndexStyle className="horn3" />
        <div onClick=handleClick style=zIndexStyle className="horn3_top" />
        <div onClick=handleClick style=zIndexStyle className="body" />
        <div onClick=handleClick style=zIndexStyle className="body_light" />
        <div
          onClick=handleClick
          style=zIndexStyle
          className="body_light2_right"
        />
      </div>
    </div>;
  },
};