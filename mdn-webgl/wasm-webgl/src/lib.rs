mod utils;

use js_sys::Float32Array;
use wasm_bindgen::{prelude::*, JsCast};
use web_sys::{WebGlBuffer, WebGlProgram, WebGlRenderingContext, WebGlUniformLocation};

struct AttribLocations {
    vertex_position: i32,
}

struct UniformLocations {
    projection_matrix: WebGlUniformLocation,
    model_view_matrix: WebGlUniformLocation,
}

struct ProgramInfo<'a> {
    program: &'a WebGlProgram,
    attrib_locations: AttribLocations,
    uniform_locations: UniformLocations,
}

impl<'a> ProgramInfo<'a> {
    fn new(program: &'a WebGlProgram, gl: &WebGlRenderingContext) -> ProgramInfo<'a> {
        ProgramInfo {
            program,
            attrib_locations: AttribLocations {
                vertex_position: gl.get_attrib_location(&program, "aVertexPosition"),
            },
            uniform_locations: UniformLocations {
                projection_matrix: gl
                    .get_uniform_location(&program, "uProjectionMatrix")
                    .expect("uniform location 'uProjectionMatrix' get failed"),
                model_view_matrix: gl
                    .get_uniform_location(&program, "uModelViewMatrix")
                    .expect("uniform location 'uModelViewMatrix' get failed"),
            },
        }
    }
}

// When the `wee_alloc` feature is enabled, use `wee_alloc` as the global
// allocator.
#[cfg(feature = "wee_alloc")]
#[global_allocator]
static ALLOC: wee_alloc::WeeAlloc = wee_alloc::WeeAlloc::INIT;

#[wasm_bindgen]
extern "C" {
    fn alert(s: &str);
}

#[wasm_bindgen]
pub fn greet() {
    alert("Hello, wasm-webgl!");
}

#[wasm_bindgen]
pub fn main() -> Result<(), JsValue> {
    utils::set_panic_hook();

    let window = web_sys::window().expect("window object is not found");
    let document = window.document().expect("document object is not found");
    let canvas = document
        .query_selector("#glCanvas")
        .expect("canvas#glCanvas is not found")
        .expect("canvas#glCanvas is not found");

    let canvas: web_sys::HtmlCanvasElement = canvas
        .dyn_into::<web_sys::HtmlCanvasElement>()
        .expect("invalid canvas object");

    let gl = canvas
        .get_context("webgl")
        .expect("webgl not working")
        .expect("webgl is falsy");

    let gl = gl
        .dyn_into::<web_sys::WebGlRenderingContext>()
        .expect("context is invalid");

    gl.clear_color(0.0, 0.0, 0.0, 1.0);
    gl.clear(web_sys::WebGlRenderingContext::COLOR_BUFFER_BIT);

    // 頂点シェーダ: main関数で組み込み変数gl_Positionに頂点データを渡す
    let vs_source = "
    attribute vec4 aVertexPosition;

    uniform mat4 uModelViewMatrix;
    uniform mat4 uProjectionMatrix;

    void main() {
      gl_Position = uProjectionMatrix * uModelViewMatrix * aVertexPosition;
    }";

    // フラグメントシェーダ: すべてのピクセルに対して一回呼び出される処理 gl_FragColorはそのピクセルの色指定
    let fs_source = "
    void main() {
      gl_FragColor = vec4(1.0, 1.0, 1.0, 1.0);
    }";

    let shader_program =
        init_shader_program(&gl, vs_source, fs_source).expect("shader program initialize failed.");
    let program_info = ProgramInfo::new(&shader_program, &gl);

    Ok(())
}

fn init_shader_program(
    gl: &web_sys::WebGlRenderingContext,
    vs_source: &str,
    fs_source: &str,
) -> Option<web_sys::WebGlProgram> {
    let vertex_shader = load_shader(gl, web_sys::WebGlRenderingContext::VERTEX_SHADER, vs_source)
        .expect("vertext shader create failed.");
    let fragment_shader = load_shader(
        gl,
        web_sys::WebGlRenderingContext::FRAGMENT_SHADER,
        fs_source,
    )
    .expect("fragment shader create failed.");

    let shader_program = gl.create_program().expect("shader program create failed.");
    gl.attach_shader(&shader_program, &vertex_shader);
    gl.attach_shader(&shader_program, &fragment_shader);
    gl.link_program(&shader_program);

    if !gl.get_program_parameter(&shader_program, web_sys::WebGlRenderingContext::LINK_STATUS) {
        alert(&format!(
            "Unable to initialize the shader program: {}",
            gl.get_program_info_log(&shader_program)
                .expect("getProgramInfoLog failed.")
        ));
        return None;
    }

    Some(shader_program)
}

fn load_shader(
    gl: &web_sys::WebGlRenderingContext,
    shader_type: u32,
    source: &str,
) -> Option<web_sys::WebGlShader> {
    let shader = gl
        .create_shader(shader_type)
        .expect(&format!("create_shader failed: {}", shader_type));

    gl.shader_source(&shader, source);

    gl.compile_shader(&shader);

    if !gl.get_shader_parameter(&shader, web_sys::WebGlRenderingContext::COMPILE_STATUS) {
        alert(&format!(
            "An error occurred compiling the shaders: {}",
            gl.get_shader_info_log(&shader)
                .expect("getProgramInfoLog failed.")
        ));
        gl.delete_shader(Some(&shader));
        return None;
    }
    Some(shader)
}

fn init_buffers(gl: &WebGlRenderingContext) -> WebGlBuffer {
    let position_buffer = gl.create_buffer();
    gl.bind_buffer(
        WebGlRenderingContext::ARRAY_BUFFER,
        position_buffer.as_ref(),
    );

    let positions: &[f32] = &[-1.0, 1.0, 1.0, 1.0, -1.0, -1.0, 1.0, -1.0];
    let typed_array = Float32Array::from(positions);

    gl.buffer_data_with_array_buffer_view(
        WebGlRenderingContext::ARRAY_BUFFER,
        &typed_array,
        WebGlRenderingContext::STATIC_DRAW,
    );
    position_buffer.expect("position_buffer create failed")
}
