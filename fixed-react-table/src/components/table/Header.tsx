import React, { createRef, useEffect, useRef } from "react";
import "../../stylesheets/Header.scss";

import { Cell } from "./Cell";

interface Props {
  height: number;
  cellWidth: number;
  colWidth: number;
  bodyWidth: number;
  totalWidth: number;
  scrollX: number;
  contents: string[];
  title: string;
}

export const Header: React.FC<Props> = (props) => {
  const momentContainer = useRef<HTMLDivElement>(null);
  useEffect(() => {
    momentContainer.current!!.scrollBy(props.scrollX, 0);
  }, [props.scrollX]);

  return (
    <div
      className="fixed-table__header"
      style={{ height: `${props.height}px` }}
    >
      <div
        className="fixed-table__header-title"
        style={{ width: `${props.colWidth}px` }}
      >
        {props.title}
      </div>
      <div
        ref={momentContainer}
        className="fixed-table__header-moments"
        style={{ width: `${props.bodyWidth}px` }}
      >
        <div
          className="fixed-table__days"
          style={{ width: `${props.totalWidth}px` }}
        >
          {props.contents.map((content, idx) => (
            <Cell key={`header-content-${idx}`} width={props.cellWidth}>
              <div>{content}</div>
            </Cell>
          ))}
        </div>
      </div>
    </div>
  );
};
