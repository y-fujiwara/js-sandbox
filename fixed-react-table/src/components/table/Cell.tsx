import React from "react";

interface Props {
  width: number;
}

export const Cell: React.FC<Props> = ({width, children}) => {
  const divStyle = {
    width: `${width}px`,
  }

  return <div style={divStyle} className="fixed-table__cell">
    {children}
  </div>;
}