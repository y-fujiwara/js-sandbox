import React, { createRef, useEffect, useRef, useState } from "react";
import { Header } from "./Header";

export const Table: React.FC = () => {
  const topContainer = useRef<HTMLDivElement>(null);
  const [bodySize, setBodySize] = useState({ width: 0, height: 0 });
  const [colHeaderWidth, setColHeaderWidth] = useState(410);
  const [scroll, setScroll] = useState({ x: 0, y: 0 });
  const calcBodySize = () => {
    setBodySize({
      height:
        window.innerHeight -
        topContainer.current!!.offsetTop -
        30 * 2 -
        36 -
        48,
      width: topContainer.current!!.clientWidth - colHeaderWidth,
    });
  };

  const resizeEvent = () => {
    calcBodySize();
  };

  // TODO: windowsだとdragイベントの方もうまく使わないとだめかも
  const wheelEvent = (e: React.WheelEvent<HTMLDivElement>) => {
    setScroll({ x: e.deltaX, y: e.deltaY });
  };

  // TODO: 要素数*セルサイズ
  const totalWidth = 100 * 30 * 2;

  useEffect(() => {
    resizeEvent();
    window.addEventListener("resize", resizeEvent);
    return () => {
      window.removeEventListener("resize", resizeEvent);
    };
  }, []);
  const start = new Date("2022-1-1");
  const end = new Date("2022-1-31");
  const days = [];
  for (const d = start; d < end; d.setDate(d.getDate() + 1)) {
    days.push(`${d.getMonth() + 1}/${d.getDate()}`);
  }
  return (
    <div
      className="fixed-table"
      ref={topContainer}
      onWheel={(e) => wheelEvent(e)}
    >
      <Header
        title={"2022/01"}
        height={30 * 2}
        cellWidth={30 * 2}
        colWidth={colHeaderWidth}
        bodyWidth={bodySize.width}
        totalWidth={totalWidth}
        scrollX={scroll.x}
        contents={days}
      />
    </div>
  );
};
