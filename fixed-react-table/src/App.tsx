import "./App.scss";
import { Table } from "./components/table/Table";

function App() {
  return (
    <main>
      <Table />
    </main>
  );
}

export default App;
