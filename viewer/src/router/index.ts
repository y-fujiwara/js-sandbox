import HelloWorld from '@/components/pages/HelloWorld.vue';
import Viewer from '@/components/pages/Viewer.vue';
import Vue from 'vue';
import Router from 'vue-router';

Vue.use(Router);

export default new Router({
  routes: [
    { path: '/', name: 'HelloWorld', component: HelloWorld }, 
    { path: '/viewer', name: 'Viewer', component: Viewer }
  ]
});
