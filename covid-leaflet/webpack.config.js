/* eslint-disable */

const path = require("path");
const HtmlWebpackPlugin = require('html-webpack-plugin')

let env = process.env.RAILS_ENV || "development";
// stagingの場合はproductionビルド
if (env === "staging") {
  env = "production";
}

module.exports = {
  entry: "./src/main.ts",
  mode: env,
  devtool: "source-map",
  output: {
    path: path.resolve(__dirname, "dist"),
    publicPath: "/dist/",
    filename: "[name].js"
  },
  resolve: {
    extensions: [".webpack.js", ".web.js", ".ts", ".tsx", ".js"]
  },
  module: {
    rules: [
      {
        test: /\.tsx?$/,
        loader: "ts-loader",
        exclude: /node_modules/
      },
      {
        test: /\.css/,
        use: [
          "style-loader",
          {
            loader: "css-loader",
            options: { url: false }
          }
        ]
      },
      {
        test: /\.(jpe?g|png|gif|mp3)$/i,
        loaders: ['file-loader?name=images/[name].[ext]']
      }
    ]
  },
  plugins: [
    new HtmlWebpackPlugin({
      template: "./src/index.html"
    })
  ],
  // ローカルサーバの設定 (webpack-dev-server 用設定)
  devServer: {
    // リロードの為のファイル監視
    watchContentBase: true,
    // コンテンツの提供元ディレクトリを設定
    // 単純に静的なページを確認したいだけなら output で出力先を設定せず、静的ファイルを置いているディレクトリを直で指定でもいい
    contentBase: path.resolve(__dirname, 'dist'),
    // ローカルサーバ立ち上げ時にブラウザで指定ページを開くかどうか (openPage で指定がない場合、index に指定したものか、ローカルサーバのトップを開く)
    open: true,
    // index ファイル (指定がない場合に標準で開くファイル) としてどれを扱うか
    index: 'index.html',
    // ビルドメッセージをブラウザコンソールに出すかどうか
    inline: true,
    // HMR (HotModuleReplacement) を有効にするかどうか
    // HMR とは: https://webpack.js.org/concepts/hot-module-replacement/
    hot: true,
    // 全てのコンテンツを gzip 圧縮かけるか
    compress: true,
    // 実行中の進捗をコンソールに出すか
    progress: true,
    // ローカルサーバの立ちあげ Port 番号 (8080 はサンプルで良く使われるので、他で使ってる場合変更する)
    port: 8080
  }
};
