import moment, { Moment } from 'moment';
import { parse } from "papaparse";

const PROVINCE_STATE = 2;
const COUNTRY_REGION = 3;
const LAST_UPDATE = 4;
const LAT = 5;
const LNG = 6;
const CONFIRMED = 7;
const DEATHS = 8;
const RECOVERED = 9;
const ACTIVE = 10;

export interface CovidResponse {
  provinceState: string;
  countryRegion: string;
  lastUpdate: Moment;
  lat: number;
  lng: number;
  confirmedNumber: number;
  deathNumber: number;
  recoverdNumber: number;
  // 上記全部の合計
  activeAmount: number;
}

/**
 * デイリーのコロナデータを取得するメソッド
 */
export const getCovidData = async (): Promise<CovidResponse[]> => {
  let subDay = 1;
  let link = getLink(subDay);
  let data = await loadCsv(link).catch(err => err);
  if (data instanceof Error && data.message === "Not Found") {
    subDay += 1;
    console.info(`CSV Data is 'Not Found'. Try to get ${subDay} ago data.`)
    link = getLink(subDay);
    data = await loadCsv(link).catch(err => console.error(err));
  } else if (data instanceof Error) {
    console.error(data);
    return [];
  }
  if (!data) return [];
  // ヘッダ行を除くのでsliceする
  data = data.slice(1);
  const result = data.filter(csvRow => csvRow[LAT] && csvRow[LNG]).map(csvRow => convertCsvToObject(csvRow));
  return result;
};

const getLink = (subDay: number) => `https://raw.githubusercontent.com/CSSEGISandData/COVID-19/master/csse_covid_19_data/csse_covid_19_daily_reports/${nowStr(subDay)}.csv`;

const nowStr = (subDay: number) => moment().subtract(subDay, "d").format("MM-DD-YYYY");

const loadCsv = (url: string): Promise<string[][]> => {
  return new Promise<string[][]>((resolve, reject) => {
    parse(url, {
      download: true,
      complete: (results: any) => {
        resolve(results.data);
      },
      error: (err: Error) => {
        reject(err);
      }
    });
  });
};

const convertCsvToObject = (csvData: string[]): CovidResponse => {
  return {
    provinceState: csvData[PROVINCE_STATE],
    countryRegion: csvData[COUNTRY_REGION],
    lastUpdate: moment(csvData[LAST_UPDATE]),
    lat: parseFloat(csvData[LAT]),
    lng: parseFloat(csvData[LNG]),
    confirmedNumber: parseInt(csvData[CONFIRMED]),
    deathNumber: parseInt(csvData[DEATHS]),
    recoverdNumber: parseInt(csvData[RECOVERED]),
    // 上記全部の合計
    activeAmount: parseInt(csvData[ACTIVE])
  }
};