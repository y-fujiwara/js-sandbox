import 'leaflet/dist/leaflet.css';
import './main.css';
import './images/layers-2x.png';
import './images/layers.png';

import L from 'leaflet';
import {getCovidData, CovidResponse} from './covidApi';

const TORA_BUILDINGS: [number, number] = [35.7025938, 139.772022];
const LICENSES: {[key: string]: string} = {
  OSM:
    '© <a href="http://osm.org/copyright">OpenStreetMap</a> contributors, <a href="http://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>',
};

const covidDataStr = (covidData: CovidResponse) => {
  return `
  <table>
    <tbody>
      <tr><td>国</td><td>${covidData.countryRegion}</td></tr>
      <tr><td>地域</td><td>${covidData.provinceState}</td></tr>
      <tr><td>更新日時</td><td>${covidData.lastUpdate.format(
        'YYYY/MM/DD hh:mm:ss',
      )}</td></tr>
      <tr><td>累計確認数</td><td>${covidData.confirmedNumber}</td></tr>
      <tr><td>累計死亡者数</td><td>${covidData.deathNumber}</td></tr>
      <tr><td>累計回復者数</td><td>${covidData.recoverdNumber}</td></tr>
    </tbody>
  </table>
  `;
};

(async function () {
  const covidData = await getCovidData();

  const customCovidLayerConfirm = covidData.map(data => {
    return L.circle([data.lat, data.lng], {
      color: 'yellow',
      fillColor: '#ffe200',
      fillOpacity: 0.5,
      radius: data.confirmedNumber * 2,
    }).bindPopup(covidDataStr(data));
  });
  const confirmLayer = L.layerGroup(customCovidLayerConfirm);

  const customCovidLayerDeath = covidData.map(data => {
    return L.circle([data.lat, data.lng], {
      color: 'red',
      fillColor: '#f03',
      fillOpacity: 0.5,
      radius: data.deathNumber * 2,
    }).bindPopup(covidDataStr(data));
  });
  const deathLayer = L.layerGroup(customCovidLayerDeath);

  const customCovidLayerRecover = covidData.map(data => {
    return L.circle([data.lat, data.lng], {
      color: 'green',
      fillColor: '#78f4ad',
      fillOpacity: 0.5,
      radius: data.recoverdNumber * 2,
    }).bindPopup(covidDataStr(data));
  });
  const recoverLayer = L.layerGroup(customCovidLayerRecover);

  // レイヤ追加(OSM)
  const osm = L.tileLayer(
    'https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png',
    {attribution: LICENSES['OSM'], maxZoom: 10},
  );
  let mymap = new L.Map('map', {maxZoom: 6}).setView(TORA_BUILDINGS, 5);
  const baseMaps = {
    OSM: osm,
  };
  const overlayMaps = {
    Confirm: confirmLayer,
    Deaths: deathLayer,
    Recover: recoverLayer,
  };

  osm.addTo(mymap);
  L.control.layers(baseMaps, overlayMaps).addTo(mymap);
})();
