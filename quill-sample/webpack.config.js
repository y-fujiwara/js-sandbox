const HtmlWebpackPlugin = require('html-webpack-plugin');

const path = require('path');

module.exports = {
  plugins: [
    new HtmlWebpackPlugin({
      // 移動させる html ファイルを指定 (このファイルに webpack でビルドされた JS や CSS が自動で設定される)
      template: './src/index.html',
    }),
  ],

  mode: 'development', // 例なので固定
  entry: {
    // /src/index.js を index.html に読込設定する
    'js/app': './src/index.js',
  },

  // 出力設定
  output: {
    // /dist に出力
    path: path.resolve(__dirname, 'dist'),
    // 公開される際の BASE URL 指定
    // ビルドされたソース等の読込設定とあっていないと読めないので注意
    // 参考: https://webpack.js.org/configuration/output/#outputpublicpath
    publicPath: '/',
    // JS の出力指定 [name] がビルドした各ファイルの名前になる
    filename: 'js/[name].js',
  },

  // ローカルサーバの設定 (webpack-dev-server 用設定)
  devServer: {
    // リロードの為のファイル監視
    watchContentBase: true,
    // コンテンツの提供元ディレクトリを設定
    // 単純に静的なページを確認したいだけなら output で出力先を設定せず、静的ファイルを置いているディレクトリを直で指定でもいい
    contentBase: path.resolve(__dirname, 'dist'),
    // ローカルサーバ立ち上げ時にブラウザで指定ページを開くかどうか (openPage で指定がない場合、index に指定したものか、ローカルサーバのトップを開く)
    open: true,
    // index ファイル (指定がない場合に標準で開くファイル) としてどれを扱うか
    index: 'index.html',
    // ビルドメッセージをブラウザコンソールに出すかどうか
    inline: true,
    // HMR (HotModuleReplacement) を有効にするかどうか
    // HMR とは: https://webpack.js.org/concepts/hot-module-replacement/
    hot: true,
    // 全てのコンテンツを gzip 圧縮かけるか
    compress: true,
    // 実行中の進捗をコンソールに出すか
    progress: true,
    // ローカルサーバの立ちあげ Port 番号 (8080 はサンプルで良く使われるので、他で使ってる場合変更する)
    port: 8080,
  },
};
