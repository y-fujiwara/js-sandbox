(module
  (import "env" "log" (func $log (param i32 i32)))

  (func $loop_test (export "loop_test") (param $n i32)
    (result i32)

    (local $i i32)
    (local $factorial i32)

    (local.set $factorial (i32.const 1))

    (loop $continue
      (block $break
        (local.set $i (i32.add (local.get $i) (i32.const 1)))
        (local.set $factorial (i32.mul (local.get $i) (local.get $factorial)))

        (call $log (local.get $i) (local.get $factorial))
        ;; $i == $nの場合はループ終了(block内のこれ以降は全部実行されない→brも実行されない)
        (br_if $break (i32.eq (local.get $i) (local.get $n)))

        ;; br_ifにマッチしなければそのままループ継続
        br $continue
      ))

      local.get $factorial
  )
)
