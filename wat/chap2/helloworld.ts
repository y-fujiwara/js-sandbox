const startStringIndex = 100;
const memory = new WebAssembly.Memory({ initial: 1 });
const importObject = {
  env: {
    buffer: memory,
    start_string: startStringIndex,
    print_string: (strLen: number) => {
      const bytes = new Uint8Array(memory.buffer, startStringIndex, strLen);
      const logString = new TextDecoder("utf8").decode(bytes);
      console.log(logString);
    },
  },
};

// 改良版(MDNに記載のinstantiateStreamingをローカルファイル読み込みで利用)
const wasmInstance = await WebAssembly.instantiateStreaming(
  fetch(new URL("./helloworld.wasm", import.meta.url).toString()),
  importObject,
);
const wasmHelloWorld = wasmInstance.instance.exports.helloworld as () => void;
wasmHelloWorld();

// もとのソースコード
// const wasmCode = await Deno.readFile("helloworld.wasm");
// const wasmModule = new WebAssembly.Module(wasmCode);

// const wasmInstance = await WebAssembly.instantiate(
//   wasmModule,
//   importObject,
// );
// const wasmHelloWorld = wasmInstance.exports.helloworld as () => void;
// wasmHelloWorld();
