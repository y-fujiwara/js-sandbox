const globalTest = null;
const memory = new WebAssembly.Memory({ initial: 1 });
const importObject = {
  js: {
    log_i32: (value: number) => {
      console.log("i32: ", value);
    },
    log_f32: (value: number) => {
      console.log("f32: ", value);
    },
    log_f64: (value: number) => {
      console.log("f64: ", value);
    },
  },
  env: {
    import_i32: 5_000_000_000,
    import_f32: 123.0123456789,
    import_f64: 123.0123456789,
  },
};

// 改良版(MDNに記載のinstantiateStreamingをローカルファイル読み込みで利用)
const wasmInstance = await WebAssembly.instantiateStreaming(
  fetch(new URL("./globals.wasm", import.meta.url).toString()),
  importObject,
);
const wasmFunc = wasmInstance.instance.exports.globaltest as () => void;
wasmFunc();
