const n = parseInt(Deno.args[0] || "1");
let loopTest = null;

const importObject = {
  env: {
    log: (n: number, factorial: number) => {
      console.log(`${n}! = ${factorial}`);
    },
  },
};

const wasmInstance = await WebAssembly.instantiateStreaming(
  fetch(new URL("./loop.wasm", import.meta.url).toString()),
  importObject,
);
const wasmFunc = wasmInstance.instance.exports.loop_test as (
  n: number,
) => number;
const factorial = wasmFunc(n);

console.log(`result ${n}! = ${factorial}`);

if (n > 12) {
  console.log(`
  ==================================================================
  Factorials greater than 12 are too large for a 32-bit integer.
  ==================================================================
  `);
}
