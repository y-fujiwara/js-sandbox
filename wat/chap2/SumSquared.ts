const value1 = parseInt(Deno.args[0]);
const value2 = parseInt(Deno.args[1]);

const wasmInstance = await WebAssembly.instantiateStreaming(
  fetch(new URL("./SumSquared.wasm", import.meta.url).toString()),
  {},
);
const wasmFunc = wasmInstance.instance.exports.SumSquared as (
  val1: number,
  val2: number,
) => number;
console.log(wasmFunc(value1, value2));
