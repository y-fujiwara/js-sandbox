(module
  ;; 組み込み環境からenvとenvが持っているprint_stringをインポート(env.print_string)
  (import "env" "print_string" (func $print_string(param i32)))
  ;; 組み込み環境からenvとenvが持っているbufferを1ページ分の線形メモリをインポート(env.buffer)
  ;; ページは線形メモリが一度に割り当て可能なブロックの最小
  (import "env" "buffer" (memory 1))
  ;; 文字列の最初のメモリ位置(MAX 65535) 最大値に近いと文字列の長さが制限される をenvが持っているstart_stringとマッピング(env.start_string)
  (global $start_string (import "env" "start_string") i32)
  ;; i32型の定数(12) 想定される文字の長さ
  (global $string_len i32 (i32.const 12))
  (data (global.get $start_string) "hello world!")
  (func (export "helloworld")
    (call $print_string (global.get $string_len))
  )
)