const MAX_MEM = 65535;

const memory = new WebAssembly.Memory({ initial: 1 });
const wasmInstance = await WebAssembly.instantiateStreaming(
  fetch(new URL("./strings.wasm", import.meta.url).toString()),
  {
    env: {
      buffer: memory,
      str_pos_len(str_pos: number, str_len: number) {
        // str_posの位置からstr_lenの長さ分を取り出す
        const bytes = new Uint8Array(memory.buffer, str_pos, str_len);
        const log_string = new TextDecoder("utf8").decode(bytes);
        console.log(log_string);
      },
      null_str(str_pos: number) {
        const bytes = new Uint8Array(memory.buffer, str_pos, MAX_MEM - str_pos);
        let log_string = new TextDecoder("utf8").decode(bytes);
        log_string = log_string.split("\0")[0];
        console.log(log_string);
      },
      len_prefix(str_pos: number) {
        const str_len = new Uint8Array(memory.buffer, str_pos, 1)[0];
        const bytes = new Uint8Array(memory.buffer, str_pos + 1, str_len);
        const log_string = new TextDecoder("utf8").decode(bytes);
        console.log(log_string);
      },
    },
  },
);
const wasmFunc = wasmInstance.instance.exports.main as () => void;
wasmFunc();
