const value = parseInt(Deno.args[0]);

const wasmInstance = await WebAssembly.instantiateStreaming(
  fetch(new URL("./is_prime.wasm", import.meta.url).toString()),
  {},
);
const wasmFunc = wasmInstance.instance.exports.is_prime as (
  val: number,
) => number;

if (wasmFunc(value)) {
  console.log(`${value} is prime!`);
} else {
  console.log(`${value} is NOT prime!`);
}
