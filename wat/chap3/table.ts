let i = 0;
const increment = () => {
  i++;
  return i;
};
const decrement = () => {
  i--;
  return i;
};

// TODO: どうやって型つけるのがいいんだろう
const importObject = {
  js: {
    tbl: null as any,
    increment,
    decrement,
    wasm_increment: null as any,
    wasm_decrement: null as any,
  },
};

const tableExport = await WebAssembly.instantiateStreaming(
  fetch(new URL("./table_export.wasm", import.meta.url).toString()),
  importObject,
);

console.log(111);

importObject.js.tbl = tableExport.instance.exports.tbl;
importObject.js.wasm_increment = tableExport.instance.exports.increment;
importObject.js.wasm_decrement = tableExport.instance.exports.decrement;

const tableTest = await WebAssembly.instantiateStreaming(
  fetch(new URL("./table_test.wasm", import.meta.url).toString()),
  importObject,
);

const { js_table_test, js_import_test, wasm_table_test, wasm_import_test } =
  tableTest.instance.exports;

i = 0;
let start = Date.now();
(js_table_test as any)();
let time = Date.now() - start;
console.log(`js_table_test time=${time}`);

i = 0;
start = Date.now();
(js_import_test as any)();
time = Date.now() - start;
console.log(`js_import_test time=${time}`);

i = 0;
start = Date.now();
(wasm_table_test as any)();
time = Date.now() - start;
console.log(`wasm_table_test time=${time}`);

i = 0;
start = Date.now();
(wasm_import_test as any)();
time = Date.now() - start;
console.log(`wasm_import_test time=${time}`);
