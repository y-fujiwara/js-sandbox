(module
  (func $even_check (param $n i32) (result i32)
    local.get $n
    i32.const 2
    i32.rem_u ;; ↑のnを2で割ったあまり
    i32.const 0
    i32.eq ;; rem_uの結果が0かどうか
  )
  (func $eq_2 (param $n i32) (result i32)
    local.get $n
    i32.const 2
    i32.eq
  )
  (func $multiple_check (param $n i32) (param $m i32) (result i32)
    local.get $n
    local.get $m
    i32.rem_u
    i32.const 0
    i32.eq
  )
  ;; 一個目のis_primeはwasm内で関数呼び出すの必要なラベルJSからだけならいらない
  (func $is_prime (export "is_prime") (param $n i32) (result i32)
    (local $i i32)
    (if (i32.eq (local.get $n) (i32.const 1))
      (then
        i32.const 0
        return
      )
    )

    (if (call $eq_2 (local.get $n))
      (then
        i32.const 1
        return
      )
    )

    (block $not_prime
      (call $even_check (local.get $n))
      ;; callの結果が0だと偶数なので素数ではない
      br_if $not_prime

      (local.set $i (i32.const 1))

      (loop $prime_test_loop
        ;; teeは$iにスタックの値を代入しつつスタックにその値を残したままにする i=i+2
        (local.tee $i (i32.add (local.get $i) (i32.const 2)))
        local.get $n
        i32.ge_u ;; $i >= $n
        ;; n以下のすべての奇数で割り切れなければ$nは素数(偶数は先頭の$event_checkでチェックされる)
        if
          i32.const 1
          return
        end

        (call $multiple_check (local.get $n) (local.get $i))

        br_if $not_prime ;; callの結果がfalse=$nが$iの倍数の場合素数ではない
        br $prime_test_loop ;; ループの先頭に戻る
      )
    )
    i32.const 0
  )
)