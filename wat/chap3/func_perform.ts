let i = 0;
let importObject = {
  js: {
    external_call: () => {
      i++;
      return i;
    },
  },
};
const wasmInstance = await WebAssembly.instantiateStreaming(
  fetch(new URL("./func_perform.wasm", import.meta.url).toString()),
  importObject,
);

const wasmCall = wasmInstance.instance.exports.wasm_call as () => number;
const jsCall = wasmInstance.instance.exports.js_call as () => number;

let start = Date.now();
wasmCall();
let time = Date.now() - start;
console.log(`wasm_call time = ${time}`);

start = Date.now();
jsCall();
time = Date.now() - start;
console.log(`js_call time = ${time}`);
