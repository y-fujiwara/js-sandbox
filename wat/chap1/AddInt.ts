const wasmCode = await Deno.readFile("AddInt.wasm");
const wasmModule = new WebAssembly.Module(wasmCode);
const wasmInstance = new WebAssembly.Instance(wasmModule);

const value1 = parseInt(Deno.args[0]);
const value2 = parseInt(Deno.args[1]);
const addInt = wasmInstance.exports
  .AddInt as (v1: number, v2: number) => number;
console.log(addInt(value1, value2).toString());
