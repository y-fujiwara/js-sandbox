(module
  (func (export "AddInt")
    (param $value_1 i32) (param $value_2 i32)
    (result i32)
    local.get $value_1
    local.get $value_2

    ;;  変数に名前つけずにインデックス番号でアクセスしていくパターン
    ;;  (param i32 i32)
    ;;  (result i32)
    ;;  local.get 0
    ;;  local.get 1

    i32.add
  )
)