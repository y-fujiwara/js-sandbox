docker container run --rm -dp 7878:7878 \
  --name=wasi-http \
  --runtime=io.containerd.wasmedge.v1 \
  --platform=wasi/wasm32 \
  zonuko/wasi-http

