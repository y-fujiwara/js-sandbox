import Vue from "vue";
import App from "./App.vue";
Vue.config.productionTip = false;
// @ts-ignore
import { createProvider } from "./vue-apollo.js";
import VueCompositionApi, { provide } from "@vue/composition-api";
import { DefaultApolloClient } from "@vue/apollo-composable";

Vue.use(VueCompositionApi);
const apolloProvider = createProvider();

// @ts-ignore
new Vue({
  render: h => h(App),

  // @ts-ignore
  setup: function() {
    provide(DefaultApolloClient, apolloProvider.defaultClient);
  }
}).$mount("#app");
