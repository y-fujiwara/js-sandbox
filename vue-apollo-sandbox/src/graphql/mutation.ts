import gql from "graphql-tag";

export const CREATE_PHOTO = gql`
  mutation createPhoto {
    createPhoto(newPhoto: { name: "test", description: "test" }) {
      id
      name
      description
      url
    }
  }
`;

export const UPDATE_PHOTO = gql`
  mutation updatePhoto($id: Int!, $input: UpdatePhoto!) {
    updatePhoto(id: $id, updatePhoto: $input) {
      id
      name
      description
      url
    }
  }
`;
