import { useQuery } from "@vue/apollo-composable";
import gql from "graphql-tag";
import { allPhotos } from "./types/allPhotos";

export const ALL_PHOTO = gql`
  query allPhotos {
    allPhotos {
      id
      name
      url
      description
    }
  }
`;
export const queryAllPhotos = () => useQuery<allPhotos>(ALL_PHOTO);
