/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { UpdatePhoto } from "./globalTypes";

// ====================================================
// GraphQL mutation operation: updatePhoto
// ====================================================

export interface updatePhoto_updatePhoto {
  __typename: "Photo";
  id: string;
  name: string;
  description: string;
  url: string;
}

export interface updatePhoto {
  updatePhoto: updatePhoto_updatePhoto;
}

export interface updatePhotoVariables {
  id: number;
  input: UpdatePhoto;
}
