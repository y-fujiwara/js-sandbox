/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL query operation: allPhotos
// ====================================================

export interface allPhotos_allPhotos {
  __typename: "Photo";
  id: string;
  name: string;
  url: string;
  description: string;
}

export interface allPhotos {
  allPhotos: allPhotos_allPhotos[];
}
