/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL mutation operation: createPhoto
// ====================================================

export interface createPhoto_createPhoto {
  __typename: "Photo";
  id: string;
  name: string;
  description: string;
  url: string;
}

export interface createPhoto {
  createPhoto: createPhoto_createPhoto[];
}
