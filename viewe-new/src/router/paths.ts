interface PathElements {
  icon: string;
  title: string;
  href: string;
  // items?: PathElements[]
}

const Paths: PathElements[] = [
  { icon: 'home', title: 'Home', href: '/' },
  { icon: 'home', title: 'Viewer', href: '/viewer' },
  { icon: 'home', title: 'About', href: '/about' },
];

export default Paths;
