mod ball;
mod bricks;

mod bitmap_container;
mod consts;
mod game_status;
mod paddle;
mod utils;

use crate::ball::Ball;
use crate::bricks::{Brick, BrickStatus};
use crate::consts::*;
use crate::game_status::{GameStatus, Status};
use crate::paddle::Paddle;

use std::cell::RefCell;
use std::rc::Rc;
use wasm_bindgen::prelude::*;
use wasm_bindgen::JsCast;
use web_sys::console;

#[cfg(feature = "wee_alloc")]
#[global_allocator]
static ALLOC: wee_alloc::WeeAlloc = wee_alloc::WeeAlloc::INIT;

#[macro_use]
extern crate serde_derive;
extern crate serde;

enum BrickEdge {
    Top,
    Bottom,
    Left,
    Right,
}

fn is_neary_edge(top_dist: f64, bottom_dist: f64, left_dist: f64, right_dist: f64) -> BrickEdge {
    if top_dist < bottom_dist && top_dist < left_dist && top_dist < right_dist {
        BrickEdge::Top
    } else if bottom_dist < top_dist && bottom_dist < left_dist && bottom_dist < right_dist {
        BrickEdge::Bottom
    } else if left_dist < top_dist && left_dist < bottom_dist && left_dist < right_dist {
        BrickEdge::Left
    } else {
        BrickEdge::Right
    }
}

pub type Bricks = Vec<Vec<Brick>>;

#[wasm_bindgen]
#[derive(Debug, Clone)]
pub struct Container {
    bricks: Vec<Vec<Brick>>,
}

#[wasm_bindgen]
impl Container {
    pub fn new() -> Container {
        let mut bricks: Bricks = Vec::new();
        bricks.resize(BRICK_COLUMN_COUNT, Vec::new());
        for c in 0..bricks.len() {
            bricks[c].resize(BRICK_ROW_COUNT, Brick::new(0.0, 0.0, BrickStatus::Live));
        }
        Container { bricks: bricks }
    }

    pub fn get_col_len(&self) -> usize {
        self.bricks.len()
    }

    pub fn get_row_len(&self, col: usize) -> usize {
        self.bricks[col].len()
    }

    pub fn get_status(&self, col: usize, row: usize) -> BrickStatus {
        self.bricks[col][row].get_status()
    }

    pub fn set_x(&mut self, col: usize, row: usize, val: f64) {
        self.bricks[col][row].set_x(val);
    }

    pub fn set_y(&mut self, col: usize, row: usize, val: f64) {
        self.bricks[col][row].set_y(val);
    }

    pub fn get_brick(&self, col: usize, row: usize) -> Brick {
        self.bricks[col][row]
    }

    pub fn set_status(&mut self, col: usize, row: usize, status: BrickStatus) {
        self.bricks[col][row].set_status(status);
    }

    pub fn draw(&mut self, ctx: &web_sys::CanvasRenderingContext2d) {
        for c in 0..self.bricks.len() {
            for r in 0..self.bricks[c].len() {
                if self.bricks[c][r].get_status() == BrickStatus::Live {
                    self.bricks[c][r].draw(&ctx, c as f64, r as f64);
                }
            }
        }
    }

    pub fn draw_with_callback(&mut self, callback: &js_sys::Function) {
        for c in 0..self.bricks.len() {
            for r in 0..self.bricks[c].len() {
                if self.bricks[c][r].get_status() == BrickStatus::Live {
                    let brick_x = c as f64 * (BRICK_WIDTH + BRICK_PADDING) + BRICK_OFFSET_LEFT;
                    let brick_y = r as f64 * (BRICK_HEIGHT + BRICK_PADDING) + BRICK_OFFSET_TOP;
                    self.bricks[c][r].set_x(brick_x);
                    self.bricks[c][r].set_y(brick_y);

                    let this = JsValue::NULL;
                    let _ = callback.call2(&this, &JsValue::from(brick_x), &JsValue::from(brick_y));
                }
            }
        }
    }

    pub fn collision_detection(&mut self, status: &mut GameStatus, ball: &mut Ball) {
        for c in 0..self.bricks.len() {
            for r in 0..self.bricks[c].len() {
                let b = self.bricks[c][r];
                if b.get_status() == BrickStatus::Live {
                    let brick_x = b.get_x();
                    let brick_y = b.get_y();
                    let x = ball.get_x();
                    let y = ball.get_y();
                    if x > brick_x
                        && x < brick_x + BRICK_WIDTH
                        && y > brick_y
                        && y < brick_y + BRICK_HEIGHT
                    {
                        ball.set_dy(-ball.get_dy());
                        ball.add_speed();
                        let is_break = self.bricks[c][r].update_status();
                        if is_break == BrickStatus::Dead {
                            status.set_score(status.get_score() + 1);
                        }
                        if status.get_score() == BRICK_SUM {
                            let _ = web_sys::window()
                                .unwrap()
                                .alert_with_message("YOU WIN, CONGRATULATIONS!");
                            let _ = web_sys::window().unwrap().location().reload();
                        }
                    }
                }
            }
        }
    }

    pub fn collision_detection_with_callback(
        &mut self,
        status: &mut GameStatus,
        ball: &mut Ball,
        break_callback: &js_sys::Function,
        win_callback: &js_sys::Function,
    ) {
        for c in 0..self.bricks.len() {
            for r in 0..self.bricks[c].len() {
                let b = self.bricks[c][r];
                if b.get_status() == BrickStatus::Live {
                    let brick_x = b.get_x();
                    let brick_y = b.get_y();
                    let x = ball.get_x();
                    let y = ball.get_y();
                    if x > brick_x
                        && x < brick_x + BRICK_WIDTH
                        && y > brick_y
                        && y < brick_y + BRICK_HEIGHT
                    {
                        // ここに来る場合中心点がいずれかのブロックの中にめり込んでいる場合なので、あとはどの辺が一番近いかチェックする
                        // TODO: 中心点の内外判定をやめたい

                        let _ =
                            break_callback.call2(&JsValue::NULL, &brick_x.into(), &brick_y.into());

                        let left_dist = (x - brick_x).abs();
                        let right_dist = (x - (brick_x + BRICK_WIDTH)).abs();
                        let bottom_dist = (y - brick_y).abs();
                        let top_dist = (y - (brick_y + BRICK_HEIGHT)).abs();
                        match is_neary_edge(top_dist, bottom_dist, left_dist, right_dist) {
                            BrickEdge::Top => ball.set_dy(-ball.get_dy()),
                            BrickEdge::Bottom => ball.set_dy(-ball.get_dy()),
                            BrickEdge::Left => ball.set_dx(-ball.get_dx()),
                            BrickEdge::Right => ball.set_dx(-ball.get_dx()),
                        }
                        ball.add_speed();
                        let is_break = self.bricks[c][r].update_status();
                        if is_break == BrickStatus::Dead {
                            status.set_score(status.get_score() + 1);
                        }
                        if status.get_score() == BRICK_SUM {
                            let this = JsValue::NULL;
                            let _ = win_callback.call1(&this, &"YOU WIN, CONGRATULATIONS!".into());
                        }
                    }
                }
            }
        }
    }
}

#[wasm_bindgen(start)]
pub fn initialize() {
    utils::set_panic_hook();
}

#[wasm_bindgen]
pub fn ball_radius() -> f64 {
    BALL_RADIUS
}

#[wasm_bindgen]
pub fn paddle_height() -> f64 {
    PADDLE_HEIGHT
}
#[wasm_bindgen]
pub fn paddle_width() -> f64 {
    PADDLE_WIDTH
}
#[wasm_bindgen]
pub fn brick_row_count() -> usize {
    BRICK_ROW_COUNT
}
#[wasm_bindgen]
pub fn brick_column_count() -> usize {
    BRICK_COLUMN_COUNT
}
#[wasm_bindgen]
pub fn brick_sum() -> u32 {
    BRICK_SUM
}

#[wasm_bindgen]
pub fn brick_width() -> f64 {
    BRICK_WIDTH
}

#[wasm_bindgen]
pub fn brick_height() -> f64 {
    BRICK_HEIGHT
}

#[wasm_bindgen]
pub fn brick_padding() -> f64 {
    BRICK_PADDING
}

#[wasm_bindgen]
pub fn brick_offset_top() -> f64 {
    BRICK_OFFSET_TOP
}

#[wasm_bindgen]
pub fn brick_offset_left() -> f64 {
    BRICK_OFFSET_LEFT
}

#[wasm_bindgen]
pub fn speed() -> f64 {
    SPEED
}
// startやると自動で実行されるみたい
#[wasm_bindgen]
pub fn start() -> Result<(), JsValue> {
    let document: web_sys::Document = web_sys::window().unwrap().document().unwrap();
    // unwrapするのでJS版のifチェックは不要(panicするので・・・)
    let canvas = document.get_element_by_id("myCanvas2").unwrap();
    let canvas: web_sys::HtmlCanvasElement = canvas
        .dyn_into::<web_sys::HtmlCanvasElement>()
        .map_err(|_| console::log_1(&JsValue::from_str("CanvasElement is invalid")))
        .unwrap();

    let context = canvas
        .get_context("2d")
        .unwrap()
        .unwrap()
        .dyn_into::<web_sys::CanvasRenderingContext2d>()
        .unwrap();

    let width = canvas.width();
    let height = canvas.height();
    let offset_left = canvas.offset_left();

    let mut bricks = Container::new();
    let mut ball = Ball::new(
        2.0,
        -2.0,
        canvas.width() as f64 / 2.0,
        canvas.height() as f64 - BALL_RADIUS * 2.0,
    );
    let status = GameStatus::new();
    let paddle = Paddle::new((canvas.width() as f64 - PADDLE_WIDTH) / 2.0);

    let f = Rc::new(RefCell::new(None));
    // イベントハンドラ内で変更して、描画処理で使うものについては参照を共有したいのでRcで作る
    let context = Rc::new(context);
    // 変更したいやつらはCellで作る
    let paddle = Rc::new(RefCell::new(paddle));
    let status = Rc::new(RefCell::new(status));

    {
        let g = f.clone();
        let context = context.clone();
        let paddle = paddle.clone();
        let status = status.clone();

        *g.borrow_mut() = Some(Closure::wrap(Box::new(move || {
            // クロージャのキャプチャで一回Moveされる
            ball.draw(&context, width as f64, height as f64);
            bricks.draw(&context);
            paddle.borrow_mut().draw(&context, height as f64);

            status.borrow().draw_score(&context);
            status.borrow().draw_lives(&context, width as f64);
            for _ in 0..ball.get_speed() {
                bricks.collision_detection(&mut status.borrow_mut(), &mut ball);
                update(
                    &mut ball,
                    &mut paddle.borrow_mut(),
                    &mut status.borrow_mut(),
                    canvas.width() as f64,
                    canvas.height() as f64,
                );
            }

            request_animation_frame(f.borrow().as_ref().unwrap());
        }) as Box<dyn FnMut()>));

        request_animation_frame(g.borrow().as_ref().unwrap());
    }

    // キーボードのキー押した時のイベント
    {
        let paddle = paddle.clone();

        let keydown_handler = Closure::wrap(Box::new(move |e: web_sys::KeyboardEvent| {
            if e.key() == "Right" || e.key() == "ArrowRight" {
                paddle.borrow_mut().set_right_pressed(true);
            } else if e.key() == "Left" || e.key() == "ArrowLeft" {
                paddle.borrow_mut().set_left_pressed(true);
            }
        }) as Box<dyn FnMut(web_sys::KeyboardEvent)>);

        document.set_onkeydown(Some(keydown_handler.as_ref().unchecked_ref()));
        keydown_handler.forget();
    }

    // キーボードのキー離したときのイベント
    {
        let paddle = paddle.clone();

        let keyup_handler = Closure::wrap(Box::new(move |e: web_sys::KeyboardEvent| {
            if e.key() == "Right" || e.key() == "ArrowRight" {
                paddle.borrow_mut().set_right_pressed(false);
            } else if e.key() == "Left" || e.key() == "ArrowLeft" {
                paddle.borrow_mut().set_left_pressed(false);
            }
        }) as Box<dyn FnMut(web_sys::KeyboardEvent)>);
        document.set_onkeyup(Some(keyup_handler.as_ref().unchecked_ref()));
        keyup_handler.forget();
    }

    // マウスイベント
    {
        let paddle = paddle.clone();

        let mousemove_handler = Closure::wrap(Box::new(move |e: web_sys::MouseEvent| {
            let relative_x = e.client_x() - offset_left;
            if relative_x > 0 && relative_x < (width as i32) {
                paddle
                    .borrow_mut()
                    .set_x(relative_x as f64 - PADDLE_WIDTH / 2.0);
            }
        }) as Box<dyn FnMut(web_sys::MouseEvent)>);
        document.set_onmousemove(Some(mousemove_handler.as_ref().unchecked_ref()));
        mousemove_handler.forget();
    }

    // クリックのスタートイベント
    {
        let status = status.clone();
        let clicke_handler = Closure::wrap(Box::new(move |_e: web_sys::MouseEvent| {
            status.borrow_mut().set_status(Status::Start);
        }) as Box<dyn FnMut(web_sys::MouseEvent)>);
        document.set_onmousemove(Some(clicke_handler.as_ref().unchecked_ref()));
        clicke_handler.forget();
    }
    Ok(())
}

fn request_animation_frame(f: &Closure<dyn FnMut()>) {
    web_sys::window()
        .unwrap()
        .request_animation_frame(f.as_ref().unchecked_ref())
        .expect("should register `requestAnimationFrame` OK");
}

#[wasm_bindgen]
pub fn update_all(
    ball: &mut Ball,
    paddle: &mut Paddle,
    status: &mut GameStatus,
    container: &mut Container,
    width: f64,
    height: f64,
    break_callback: &js_sys::Function,
    alert_callback: &js_sys::Function,
) {
    // TODO: ここのループは固定回数にしてスピードの加算でなんとかしたい
    for _ in 0..ball.get_speed() {
        container.collision_detection_with_callback(status, ball, break_callback, alert_callback);
        update_with_callback(ball, paddle, status, width, height, alert_callback);
    }
}

pub fn update(
    ball: &mut Ball,
    paddle: &mut Paddle,
    status: &mut GameStatus,
    width: f64,
    height: f64,
) {
    if ball.get_x() + ball.get_dx() > width as f64 - BALL_RADIUS
        || ball.get_x() + ball.get_dx() < BALL_RADIUS
    {
        ball.set_dx(-ball.get_dx());
    }

    if ball.get_y() + ball.get_dy() < BALL_RADIUS {
        ball.set_dy(-ball.get_dy());
    } else if ball.get_y() + ball.get_dy() > height as f64 - BALL_RADIUS {
        if ball.get_x() > paddle.get_x() && ball.get_x() < paddle.get_x() + PADDLE_WIDTH {
            // パドルにボールが当たった場合
            let dist = (ball.get_x() + BALL_RADIUS) - (paddle.get_x() + PADDLE_WIDTH / 2.0);
            let radian = (90.0 - (dist / (PADDLE_WIDTH / 2.0)) * 80.0).to_radians();
            let speed = (ball.get_dx().powf(2.0) + ball.get_dy().powf(2.0)).sqrt();
            ball.set_dx(radian.cos() * speed);
            ball.set_dy(-radian.sin() * speed);
            ball.add_speed();
        } else {
            status.set_status(Status::Stop);
            status.set_lives(status.get_lives() - 1);
            if status.get_lives() == 0 {
                let _ = web_sys::window().unwrap().alert_with_message("GAME OVER");
                let _ = web_sys::window().unwrap().location().reload();
            } else {
                ball.set_x(width as f64 / 2.0);
                ball.set_y(height as f64 - BALL_RADIUS);
                ball.set_dx(2.0 * SPEED);
                ball.set_dy(-2.0 * SPEED);
                ball.init_speed();
                paddle.set_x((width as f64 - PADDLE_WIDTH) / 2.0);
            }
        }
    }
    if paddle.get_right_pressed() && paddle.get_x() < width as f64 - PADDLE_WIDTH {
        paddle.set_x(paddle.get_x() + 7.0);
    } else if paddle.get_left_pressed() && paddle.get_x() > 0.0 {
        paddle.set_x(paddle.get_x() - 7.0);
    }
    ball.set_x(ball.get_x() + ball.get_dx());
    ball.set_y(ball.get_y() + ball.get_dy());
}

pub fn update_with_callback(
    ball: &mut Ball,
    paddle: &mut Paddle,
    status: &mut GameStatus,
    width: f64,
    height: f64,
    callback: &js_sys::Function,
) {
    if status.get_status() != Status::Start {
        return;
    }
    if ball.get_x() + ball.get_dx() > width as f64 - BALL_RADIUS
        || ball.get_x() + ball.get_dx() < BALL_RADIUS
    {
        // 壁にあたった場合その１
        ball.set_dx(-ball.get_dx());
    }

    if ball.get_y() + ball.get_dy() < BALL_RADIUS {
        // 壁にあたった場合その2
        ball.set_dy(-ball.get_dy());
    } else if ball.get_y() + ball.get_dy() > height as f64 - BALL_RADIUS {
        if ball.get_x() > paddle.get_x() && ball.get_x() < paddle.get_x() + PADDLE_WIDTH {
            // パドルにボールが当たった場合
            let dist = (ball.get_x() + BALL_RADIUS) - (paddle.get_x() + PADDLE_WIDTH / 2.0);
            let radian = (90.0 - (dist / (PADDLE_WIDTH / 2.0)) * 80.0).to_radians();
            let speed = (ball.get_dx().powf(2.0) + ball.get_dy().powf(2.0)).sqrt();
            ball.set_dx(radian.cos() * speed);
            ball.set_dy(-radian.sin() * speed);
            ball.add_speed();
        } else {
            ball.set_dy(-ball.get_dy());
            // ここのelse節は下に突き抜けた場合
            status.set_status(Status::Stop);
            status.set_lives(status.get_lives() - 1);
            if status.get_lives() == 0 {
                let this = JsValue::NULL;
                let _ = callback.call1(&this, &"GAME OVER".into());
            } else {
                ball.set_x(width as f64 / 2.0);
                ball.set_y(height as f64 - BALL_RADIUS * 2.0);
                ball.set_dx(2.0 * SPEED);
                ball.set_dy(-2.0 * SPEED);
                ball.init_speed();
                paddle.set_x((width as f64 - PADDLE_WIDTH) / 2.0);
            }
        }
    }
    if paddle.get_right_pressed() && paddle.get_x() < width as f64 - PADDLE_WIDTH {
        paddle.set_x(paddle.get_x() + 7.0);
    } else if paddle.get_left_pressed() && paddle.get_x() > 0.0 {
        paddle.set_x(paddle.get_x() - 7.0);
    }
    ball.set_x(ball.get_x() + ball.get_dx());
    ball.set_y(ball.get_y() + ball.get_dy());
}
