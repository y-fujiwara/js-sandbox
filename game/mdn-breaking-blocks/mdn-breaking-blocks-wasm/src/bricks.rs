use crate::consts::{
    BRICK_HEIGHT, BRICK_OFFSET_LEFT, BRICK_OFFSET_TOP, BRICK_PADDING, BRICK_WIDTH,
};
use wasm_bindgen::prelude::*;

#[wasm_bindgen]
#[derive(Debug, Clone, Copy, Serialize, PartialEq, Eq)]
#[repr(u8)]
pub enum BrickStatus {
    Live,
    Dead,
}

#[wasm_bindgen]
#[derive(Debug, Clone, Copy, Serialize)]
pub struct Brick {
    x: f64,
    y: f64,
    status: BrickStatus,
    life: u32,
}

#[wasm_bindgen]
impl Brick {
    pub fn new(x: f64, y: f64, status: BrickStatus) -> Brick {
        Brick {
            x: x,
            y: y,
            status: status,
            life: 1,
        }
    }

    pub fn get_x(&self) -> f64 {
        self.x
    }

    pub fn set_x(&mut self, x: f64) {
        self.x = x;
    }

    pub fn get_y(&self) -> f64 {
        self.y
    }

    pub fn set_y(&mut self, y: f64) {
        self.y = y;
    }

    pub fn set_status(&mut self, status: BrickStatus) {
        self.status = status;
    }

    pub fn get_status(&self) -> BrickStatus {
        self.status
    }

    pub fn update_status(&mut self) -> BrickStatus {
        self.life -= self.life;
        if self.life == 0 {
            self.status = BrickStatus::Dead;
        }
        self.status
    }

    pub fn draw(&mut self, ctx: &web_sys::CanvasRenderingContext2d, c: f64, r: f64) {
        self.x = c * (BRICK_WIDTH + BRICK_PADDING) + BRICK_OFFSET_LEFT;
        self.y = r * (BRICK_HEIGHT + BRICK_PADDING) + BRICK_OFFSET_TOP;
        ctx.begin_path();
        ctx.rect(self.x, self.y, BRICK_WIDTH, BRICK_HEIGHT);
        ctx.set_fill_style(&"rgb(0, 149, 208)".into());
        ctx.fill();
        ctx.close_path();
    }
}
