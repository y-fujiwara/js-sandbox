use futures::{future, Future};
use js_sys::Promise;
use wasm_bindgen::prelude::*;
use wasm_bindgen::JsCast;
use wasm_bindgen_futures::{future_to_promise, JsFuture};
use web_sys::{
    DedicatedWorkerGlobalScope, ImageBitmap, Request, RequestInit, RequestMode, Response,
};

#[wasm_bindgen]
#[derive(Debug, Clone)]
pub struct BitmapContainer {
    bitmap: Vec<ImageBitmap>,
}

#[wasm_bindgen]
impl BitmapContainer {
    pub fn new() -> BitmapContainer {
        BitmapContainer { bitmap: Vec::new() }
    }

    pub fn get_image(
        &mut self,
        worker: &DedicatedWorkerGlobalScope,
        col: u32,
        row: u32,
        url: String,
    ) -> Promise {
        let mut opts = RequestInit::new();
        opts.method("GET");
        opts.mode(RequestMode::Cors);
        let request = Request::new_with_str_and_init(
            &format!("{}imgs/27_devil_{}_{}.png", url, row + 1, col + 1),
            &opts,
        )
        .unwrap();
        let request_promise = worker.fetch_with_request(&request);

        let future = JsFuture::from(request_promise)
            .and_then(|resp_value| {
                // `resp_value` is a `Response` object.
                assert!(resp_value.is_instance_of::<Response>());
                let resp: Response = resp_value.dyn_into().unwrap();
                // worker.create_image_bitmap_with_blob(resp.blob())
                resp.blob()
            })
            .and_then(|blob: Promise| JsFuture::from(blob))
            .and_then(|blob| future::ok(blob));

        future_to_promise(future)
    }

    pub fn set_bitmap(&mut self, image: ImageBitmap) {
        self.bitmap.push(image);
    }

    // pub fn get_bitmap(&self, idx: usize) -> ImageBitmap {
    //     self.bitmap[idx].into()
    // }
}
