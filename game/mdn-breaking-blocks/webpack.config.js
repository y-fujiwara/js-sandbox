/* eslint-disable @typescript-eslint/no-var-requires */
const path = require("path");
const HtmlWebpackPlugin = require("html-webpack-plugin");
const webpack = require("webpack");
const WasmPackPlugin = require("@wasm-tool/wasm-pack-plugin");
const WorkerPlugin = require("worker-plugin");
const ASSET_PATH = process.env.ASSET_PATH || "";

module.exports = {
  resolve: {
    extensions: [".ts", ".tsx", ".js", ".jsx", ".wasm"]
  },
  output: {
    globalObject: "this",
    publicPath: ASSET_PATH
  },
  module: {
    rules: [
      {
        test: /\.tsx?$/,
        loader: "ts-loader",
        options: {
          transpileOnly: true
        }
      },
      {
        test: /\.(jpg|png)$/,
        loader: "file-loader?name=[name].[ext]"
      }
    ]
  },
  plugins: [
    new HtmlWebpackPlugin({
      template: path.join(__dirname, "src/index.html")
    }),
    new WasmPackPlugin({
      crateDirectory: path.join(__dirname, "mdn-breaking-blocks-wasm"),
      outName: "mdn_breaking_blocks_wasm",
      withTypeScript: true
    }),
    // Have this example work in Edge which doesn"t ship `TextEncoder` or
    // `TextDecoder` at this time.
    new webpack.ProvidePlugin({
      TextDecoder: ["text-encoding", "TextDecoder"],
      TextEncoder: ["text-encoding", "TextEncoder"]
    }),
    // This makes it possible for us to safely use env vars on our code
    new webpack.DefinePlugin({
      "process.env.ASSET_PATH": JSON.stringify(ASSET_PATH),
      ASSET_URL: JSON.stringify(ASSET_PATH)
    }),
    new WorkerPlugin({
      plugins: [
        "WasmPackPlugin"
        // new webpack.DefinePlugin({
        //   "process.env.ASSET_PATH": JSON.stringify(ASSET_PATH),
        //   ASSET_URL: JSON.stringify(ASSET_PATH)
        // })
      ]
    })
  ]
};
