import { BreakingBlocksTask } from "./tasks/breaking-blocks.task";
import { LayerTask } from "./tasks/layer.task";

const logFunc = (msg: string) => {
  // eslint-disable-next-line no-console
  console.log(msg);
};

const alertFunc = (msg: string) => {
  alert(msg);
  document.location.reload();
};

export class Main {
  private myWorker: BreakingBlocksTask;
  private myLayer: LayerTask;

  public constructor() {
    this.myWorker = new BreakingBlocksTask();
    this.setupMainCanvas();
    this.myLayer = new LayerTask();
    const layer = document.getElementById(
      "myCanvas-layer"
    ) as HTMLCanvasElement;
    const offscreenLayer = layer.transferControlToOffscreen() as any;

    this.myLayer.sendQuery("init", {
      transfers: [offscreenLayer],
      offscreen: offscreenLayer
    });
  }

  private setupMainCanvas() {
    this.myWorker.addListeners("log", logFunc);

    this.myWorker.addListeners("alert", alertFunc);
    this.myWorker.addListeners(
      "collisionCallback",
      this.collisionCallback.bind(this)
    );

    this.myWorker.addListeners("setEvent", () => {
      document.addEventListener(
        "keydown",
        this.createEvent("keydown").bind(this),
        false
      );
      document.addEventListener(
        "keyup",
        this.createEvent("keyup").bind(this),
        false
      );
      document.addEventListener(
        "mousemove",
        this.createEvent("mousemove").bind(this),
        false
      );
      document.addEventListener(
        "click",
        this.createEvent("click").bind(this),
        false
      );
    });
  }

  private createEvent(func: string) {
    return (e: KeyboardEvent | MouseEvent) => {
      if (e instanceof KeyboardEvent) {
        this.myWorker.sendQuery(func, { key: e.key });
      } else {
        this.myWorker.sendQuery(func, { key: e.clientX });
      }
    };
  }

  private collisionCallback(
    brickX: number,
    brickY: number,
    c: number,
    r: number
  ) {
    this.myLayer.sendQuery("drawImageCell", {
      brickX: brickX,
      brickY: brickY,
      c: c,
      r: r
    });
  }

  public async show() {
    const canvas = document.getElementById("myCanvas") as HTMLCanvasElement;
    const offscreen = canvas.transferControlToOffscreen() as any;
    this.myWorker.sendQuery("drawStart", {
      transfers: [offscreen],
      offscreen: offscreen,
      offsetLeft: canvas.offsetLeft
    });
  }
}
