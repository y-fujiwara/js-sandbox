import { Main } from "./main";

function noticeNotUsingChrome() {
  const container = document.getElementById("canvas-container");
  if (!container) return;
  container.innerHTML = `
  <div style="text-align: center;" >
    <strong>
      <p>本アプリケーションはMac/Windows版Google Chromeでのみ動作を確認しております。</p>
      <p>お使いのブラウザまたは端末を変更し、お試しください。</p>
    </strong>
    <div class="backStr">sample</div>
    <video src="${ASSET_URL}/imgs/breaking_blocks.mp4" playsinline muted autoplay loop></video>
  </div>
    `;
}

(async function() {
  const agent = window.navigator.userAgent.toLowerCase();
  const chrome =
    agent.indexOf("chrome") !== -1 &&
    agent.indexOf("edge") === -1 &&
    agent.indexOf("opr") === -1;

  const pc =
    agent.indexOf("android") === -1 &&
    agent.indexOf("mobile") === -1 &&
    agent.indexOf("iphone") === -1;

  if (!chrome || !pc) {
    noticeNotUsingChrome();
    return;
  }
  const main = new Main();
  await main.show();
})();
