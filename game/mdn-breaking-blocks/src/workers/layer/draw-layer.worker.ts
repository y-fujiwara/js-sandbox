// import axios from "axios";

interface LogicsInterface {
  init: (canvas: OffscreenCanvas) => void;
  drawImageCell: (brickX: number, brickY: number, c: number, r: number) => void;
  canvas?: OffscreenCanvas;
}

export const logics: LogicsInterface = {
  init(canvas: OffscreenCanvas) {
    Object.assign(logics, { canvas: canvas });
  },

  async drawImageCell(brickX: number, brickY: number, c: number, r: number) {
    if (this.canvas && this.canvas instanceof OffscreenCanvas) {
      const ctxImage = this.canvas.getContext("2d");
      const imagePath = `${ASSET_URL}imgs/26_engel_${r + 1}_${c + 1}.png`;
      if (ctxImage && ctxImage instanceof OffscreenCanvasRenderingContext2D) {
        const res = await fetch(imagePath);
        const blob = await res.blob();

        const imageBitmap = await createImageBitmap(blob);
        ctxImage.drawImage(imageBitmap, brickX, brickY, 48, 20);
      }
    }
  }
};
