import { logics } from "./draw-layer.worker";

/* eslint-disable no-prototype-builtins */
/* eslint-disable @typescript-eslint/camelcase */

// TSで重複とか言われるのを防ぐために無意味なexportなどを行う
// export default 0;

// システム関数
function defaultReply(message: any) {
  postMessage({
    queryMethodListener: "log",
    queryMethodArguments: [message]
  });
}

function reply(...args: any) {
  if (args.length < 1) {
    throw new TypeError("reply - not enough arguments");
  }
  postMessage({
    queryMethodListener: args[0],
    queryMethodArguments: Array.prototype.slice.call(args, 1)
  });
}

const queryableFunctions: { [key: string]: any } = {
  drawImageCell: async (
    brickX: number,
    brickY: number,
    c: number,
    r: number
  ) => {
    logics.drawImageCell(brickX, brickY, c, r);
  },
  init: (canvas: OffscreenCanvas) => {
    logics.init(canvas);
  }
};

onmessage = function(oEvent) {
  if (
    oEvent.data instanceof Object &&
    oEvent.data.hasOwnProperty("queryMethod") &&
    oEvent.data.hasOwnProperty("queryMethodArguments")
  ) {
    queryableFunctions[oEvent.data.queryMethod].apply(
      self,
      oEvent.data.queryMethodArguments
    );
  } else {
    defaultReply(oEvent.data);
  }
};
