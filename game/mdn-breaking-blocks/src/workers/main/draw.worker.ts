/* eslint-disable no-prototype-builtins */
/* eslint-disable @typescript-eslint/camelcase */
import {
  speed,
  brick_width,
  brick_height,
  paddle_height,
  paddle_width,
  ball_radius,
  GameStatus,
  Paddle,
  Ball,
  Container,
  Status,
  Brick,
  BitmapContainer,
  update_all,
  brick_row_count,
  brick_offset_top,
  brick_column_count,
  brick_offset_left,
  brick_padding
} from "mdn-breaking-blocks-wasm";

const brick = Brick.new(1, 1, 0);

const toHex = (v: number): string => {
  return ("00" + v.toString(16).toUpperCase()).substr(-2);
};

function hsl2rgb(h: number, s: number, l: number) {
  s = s / 100;
  l = l / 100;
  let rgb = [0, 0, 0];
  const c = (1 - Math.abs(2 * l - 1)) * s;
  const x = c * (1 - Math.abs(((h / 60) % 2) - 1));
  const m = l - c / 2;
  if (h >= 0 && h < 60) rgb = [c, x, 0];
  if (h >= 60 && h < 120) rgb = [x, c, 0];
  if (h >= 120 && h < 180) rgb = [0, c, x];
  if (h >= 180 && h < 240) rgb = [0, x, c];
  if (h >= 240 && h < 300) rgb = [x, 0, c];
  if (h >= 300 && h < 360) rgb = [c, 0, x];
  return rgb.map(v => (255 * (v + m)) | 0);
}

export class DrawWorker {
  private _events: {
    [key: string]: (val: any) => void;
  } = {};
  public get events(): {
    [key: string]: (val: any) => void;
  } {
    return this._events;
  }
  private reply: (...msg: any) => void;

  private ball: Ball;
  private paddle: Paddle;
  private status: GameStatus;
  private container: Container;
  private bitmapContainer: BitmapContainer;
  private requestId: number = NaN;
  private readonly h = 240;
  private readonly frameTime = 1 / 30;
  private prevTimestamp = 0;
  private bitmapArray: { [idx: string]: ImageBitmap } = {};

  public constructor(canvas: OffscreenCanvas, reply: (msg: any) => void) {
    this.reply = reply;
    this.status = GameStatus.new();
    this.container = Container.new();
    this.bitmapContainer = BitmapContainer.new();

    this.ball = Ball.new(
      2 * speed(),
      -2 * speed(),
      canvas.width / 2,
      canvas.height - ball_radius() * 2.0
    );
    this.paddle = Paddle.new((canvas.width - paddle_width()) / 2);
  }

  public async bitmapInit() {
    for (let i = 0; i < brick_column_count(); i++) {
      for (let j = 0; j < brick_row_count(); j++) {
        this.bitmapContainer
          .get_image(self, i, j, ASSET_URL)
          .then(async (res: Blob) => {
            const bitmap = await createImageBitmap(res);
            this.bitmapArray[`${i}-${j}`] = bitmap;
          });
      }
    }
  }

  private drawBall(
    ctx: OffscreenCanvasRenderingContext2D,
    width: number,
    height: number
  ) {
    ctx.clearRect(0, 0, width, height);
    ctx.beginPath();
    ctx.strokeStyle = "black";
    ctx.lineWidth = 0.3;
    ctx.arc(
      this.ball.get_x(),
      this.ball.get_y(),
      ball_radius(),
      0,
      Math.PI * 2
    );
    ctx.fillStyle = "#FFFFFF";
    ctx.fill();
    ctx.stroke();
    ctx.closePath();
  }

  private drawBricks(ctx: OffscreenCanvasRenderingContext2D) {
    this.container.draw_with_callback((brickX: number, brickY: number) => {
      ctx.beginPath();
      // ctx.rect(args.brickX, args.brickY, brick_width(), brick_height());
      // const [r, g, b] = hsl2rgb(args.brickY % 360, 80, 50);
      // ctx.fillStyle = `#${toHex(r)}${toHex(g)}${toHex(b)}`;
      // ctx.fill();
      // let brick_x = c  * (BRICK_WIDTH + BRICK_PADDING) + BRICK_OFFSET_LEFT;
      // let brick_y = r  * (BRICK_HEIGHT + BRICK_PADDING) + BRICK_OFFSET_TOP;
      const c =
        (brickX - brick_offset_left()) / (brick_width() + brick_padding());
      const r =
        (brickY - brick_offset_top()) / (brick_height() + brick_padding());
      ctx.drawImage(this.bitmapArray[`${c}-${r}`], brickX, brickY, 48, 20);
      ctx.strokeStyle = "#5f5f5f";
      ctx.lineWidth = 0.5;
      ctx.strokeRect(brickX, brickY, brick_width(), brick_height());
      ctx.closePath();
    });
  }

  private drawPaddle(ctx: OffscreenCanvasRenderingContext2D, height: number) {
    {
      ctx.beginPath();
      ctx.fillStyle = "#f80";

      ctx.strokeStyle = "#5f5f5f";
      ctx.lineWidth = 0.5;
      ctx.strokeRect(
        this.paddle.get_x(),
        height - paddle_height(),
        paddle_width(),
        paddle_height()
      );

      ctx.rect(
        this.paddle.get_x(),
        height - paddle_height(),
        paddle_width(),
        paddle_height()
      );
      ctx.fill();
      ctx.closePath();
    }
  }

  private drawScore(ctx: OffscreenCanvasRenderingContext2D) {
    ctx.font = "16px Arial";
    ctx.fillStyle = "#0095DD";
    ctx.fillText(`Score: ${this.status.get_score()}`, 8, 20);
  }

  private drawLives(ctx: OffscreenCanvasRenderingContext2D, width: number) {
    ctx.font = "16px Arial";
    ctx.fillStyle = "#0095DD";
    ctx.fillText(`Lives: ${this.status.get_lives()}`, width - 65, 20);
  }

  private update(width: number, height: number) {
    update_all(
      this.ball,
      this.paddle,
      this.status,
      this.container,
      width,
      height,
      (brickX: number, brickY: number) => {
        const c =
          (brickX - brick_offset_left()) / (brick_width() + brick_padding());
        const r =
          (brickY - brick_offset_top()) / (brick_height() + brick_padding());

        this.reply("collisionCallback", brickX, brickY, c, r);
      },
      (msg: string) => {
        this.reply("alert", msg);
        cancelAnimationFrame(this.requestId);
        this.reply("terminate");
      }
    );
  }

  private initCtx(canvas: OffscreenCanvas) {
    const ctx = canvas.getContext("2d");
    if (ctx === null || !(ctx instanceof OffscreenCanvasRenderingContext2D)) {
      this.reply("terminate");
      return null;
    }
    // ctx.shadowColor = "#555";
    // ctx.shadowOffsetX = 2;
    // ctx.shadowOffsetY = 1;
    // ctx.shadowBlur = 3;
    // ctx.save();
    return ctx;
  }

  public async drawStart(
    canvas: OffscreenCanvas,
    offsetLeft: number,
    readyCallback: (event: { [key: string]: (val: any) => void }) => void
  ) {
    const ctx = this.initCtx(canvas);
    if (!ctx) return;

    const draw = (timestamp: DOMHighResTimeStamp) => {

      // requestAnimationFrameがupdateを呼び出す頻度が
      // FPSで指定したフレーム数よりも多い場合は
      // 画面の描きなおしをせずに次のrequestAnimationFrameを実行する
      const elapsed = (timestamp - this.prevTimestamp) / 1000;
      if (elapsed <= this.frameTime) {
        this.requestId = requestAnimationFrame(draw);
        return;
      }
      this.prevTimestamp = timestamp;
      // ctx.shadowOffsetX = 2;
      // ctx.shadowOffsetY = 1;
      // ctx.shadowBlur = 3;
      this.drawBall(ctx, canvas.width, canvas.height);
      this.drawBricks(ctx);
      this.drawPaddle(ctx, canvas.height);

      ctx.shadowOffsetX = 0;
      ctx.shadowOffsetY = 0;
      ctx.shadowBlur = 0;
      this.drawScore(ctx);
      this.drawLives(ctx, canvas.width);
      this.update(canvas.width, canvas.height);

      this.requestId = requestAnimationFrame(draw);
    };

    this._events = {
      keydown: (key: string) => {
        if (key === "Right" || key === "ArrowRight") {
          this.paddle.set_right_pressed(true);
        } else if (key === "Left" || key === "ArrowLeft") {
          this.paddle.set_left_pressed(true);
        }
      },

      keyup: (key: string) => {
        if (key === "Right" || key === "ArrowRight") {
          this.paddle.set_right_pressed(false);
        } else if (key === "Left" || key === "ArrowLeft") {
          this.paddle.set_left_pressed(false);
        }
      },
      mousemove: (clientX: number) => {
        const relativeX = clientX - offsetLeft;
        if (relativeX > 0 && relativeX < canvas.width) {
          this.paddle.set_x(relativeX - paddle_width() / 2);
          if (this.status.get_status() !== Status.Start) {
            this.ball.set_x(relativeX);
          }
        }
      },
      click: () => {
        this.status.set_status(Status.Start);
      }
    };
    readyCallback(this.events);
    draw();
  }
}
