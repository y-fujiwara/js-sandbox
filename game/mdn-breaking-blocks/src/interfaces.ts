export interface Listener {
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  (...args: any): void;
}
