/* eslint-disable no-prototype-builtins */
import { Listener } from "../interfaces";

export class LayerTask {
  private worker: Worker;
  private defaultListener: Listener = () => {};
  private listeners: { [keys: string]: Listener } = {
    terminate: () => {
      this.terminate();
    }
  };

  public constructor(onError = null, defaultListener?: Listener) {
    if (defaultListener) this.defaultListener = defaultListener;
    this.worker = new Worker("../workers/layer/layer.worker.ts", {
      name: "layer",
      type: "module"
    });
    if (onError) this.worker.onerror = onError;
    this.worker.onmessage = event => {
      // callで呼ぶのは、dataのJSONにhasOwnPropertyを仕込まれると脆弱性になるため
      if (
        event.data instanceof Object &&
        event.data.hasOwnProperty("queryMethodListener") &&
        event.data.hasOwnProperty("queryMethodArguments")
      ) {
        this.listeners[event.data.queryMethodListener].apply(
          this,
          event.data.queryMethodArguments
        );
      } else {
        this.defaultListener(this, event.data);
      }
    };
  }

  public postMessage(message: string) {
    this.worker.postMessage(message);
  }

  public terminate() {
    this.worker.terminate();
  }

  public addListeners(name: string, listener: Listener) {
    this.listeners[name] = listener;
  }

  public removeListeners(name: string) {
    delete this.listeners[name];
  }

  public async sendQuery(
    method: string,
    {
      transfers,
      ...args
    }: { transfers?: Transferable[] | null; [key: string]: any }
  ) {
    const arrayArgs = Object.keys(args).map(function(key) {
      return args[key];
    });
    if (!method) {
      throw new TypeError("sendQuery takes at least one argument");
      return;
    }
    if (!transfers) {
      this.worker.postMessage({
        queryMethod: method,
        queryMethodArguments: arrayArgs
      });
    } else {
      this.worker.postMessage(
        {
          queryMethod: method,
          queryMethodArguments: arrayArgs
        },
        transfers
      );
    }
  }
}
