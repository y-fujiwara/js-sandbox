/* eslint-disable @typescript-eslint/camelcase */
import {
  start,
  speed,
  build_bricks_conv,
  Brick,
  brick_width,
  brick_height,
  brick_column_count,
  brick_offset_left,
  brick_offset_top,
  brick_padding,
  brick_row_count,
  paddle_height,
  paddle_width,
  ball_radius
} from "mdn-breaking-blocks-wasm";

interface Paddle {
  x: number;
  rightPressed: boolean;
  leftPressed: boolean;
}

interface Ball {
  dy: number;
  dx: number;
  x: number;
  y: number;
}

interface GameStatus {
  score: number;
  lives: number;
}

const drawBall = (
  ctx: CanvasRenderingContext2D,
  width: number,
  height: number,
  ball: Ball
) => {
  ctx.clearRect(0, 0, width, height);
  ctx.beginPath();
  ctx.arc(ball.x, ball.y, ball_radius(), 0, Math.PI * 2);
  ctx.fillStyle = "#0095DD";
  ctx.fill();
  ctx.closePath();
};

const drawBricks = (ctx: CanvasRenderingContext2D, bricks: Brick[][]) => {
  for (let c = 0; c < bricks.length; c++) {
    for (let r = 0; r < bricks[c].length; r++) {
      // 未衝突ブロックのみ再描画
      if (bricks[c][r].status == 1) {
        const brickX =
          c * (brick_width() + brick_padding()) + brick_offset_left();
        const brickY =
          r * (brick_height() + brick_padding()) + brick_offset_top();
        bricks[c][r].x = brickX;
        bricks[c][r].y = brickY;
        ctx.beginPath();
        ctx.rect(brickX, brickY, brick_width(), brick_height());
        ctx.fillStyle = "#0095DD";
        ctx.fill();
        ctx.closePath();
      }
    }
  }
};

const drawPaddle = (
  ctx: CanvasRenderingContext2D,
  paddle: Paddle,
  height: number
) => {
  ctx.beginPath();
  ctx.rect(paddle.x, height - paddle_height(), paddle_width(), paddle_height());
  ctx.fillStyle = "#0095D";
  ctx.fill();
  ctx.closePath();
};

const drawScore = (ctx: CanvasRenderingContext2D, score: number) => {
  ctx.font = "16px Arial";
  ctx.fillStyle = "#0095DD";
  ctx.fillText(`Score: ${score}`, 8, 20);
};

const drawLives = (
  ctx: CanvasRenderingContext2D,
  lives: number,
  width: number
) => {
  ctx.font = "16px Arial";
  ctx.fillStyle = "#0095DD";
  ctx.fillText(`Lives: ${lives}`, width - 65, 20);
};

const collisionDetection = (
  bricks: Brick[][],
  ball: Ball,
  status: GameStatus
) => {
  for (let c = 0; c < brick_column_count(); c++) {
    for (let r = 0; r < brick_row_count(); r++) {
      const b = bricks[c][r];
      if (b.status == 1) {
        if (
          ball.x > b.x &&
          ball.x < b.x + brick_width() &&
          ball.y > b.y &&
          ball.y < b.y + brick_height()
        ) {
          ball.dy = -ball.dy;
          b.status = 0;
          status.score++;
          if (status.score === brick_row_count() * brick_column_count()) {
            alert("YOU WIN, CONGRATULATIONS!");
            document.location.reload();
          }
        }
      }
    }
  }
};

const update = (
  width: number,
  height: number,
  ball: Ball,
  status: GameStatus,
  paddle: Paddle
) => {
  if (
    ball.x + ball.dx > width - ball_radius() ||
    ball.x + ball.dx < ball_radius()
  ) {
    ball.dx = -ball.dx;
  }
  if (ball.y + ball.dy < ball_radius()) {
    ball.dy = -ball.dy;
  } else if (ball.y + ball.dy > height - ball_radius()) {
    // y軸の一番下についたときにパドルの幅の間ならパドルに跳ね返される
    if (ball.x > paddle.x && ball.x < paddle.x + paddle_width()) {
      ball.dy = -ball.dy;
    } else {
      status.lives--;
      if (!status.lives) {
        alert("GAME OVER");
        document.location.reload();
      } else {
        ball.x = width / 2;
        ball.y = height - 30;
        ball.dx = 2 * speed();
        ball.dy = -2 * speed();
        paddle.x = (width - paddle_width()) / 2;
      }
    }
  }

  if (paddle.rightPressed && paddle.x < width - paddle_width()) {
    paddle.x += 7;
  } else if (paddle.leftPressed && paddle.x > 0) {
    paddle.x -= 7;
  }
  ball.x += ball.dx;
  ball.y += ball.dy;
};

export const run = () => {
  const canvas = document.getElementById("myCanvas");
  if (!(canvas instanceof HTMLCanvasElement)) {
    return;
  }

  const ctx = canvas.getContext("2d");
  if (ctx === null) return;

  const ball: Ball = {
    dy: 2 * speed(),
    dx: -2 * speed(),
    x: canvas.width / 2,
    y: canvas.height - 30
  };

  const paddle: Paddle = {
    x: (canvas.width - paddle_width()) / 2,
    rightPressed: false,
    leftPressed: false
  };

  const status: GameStatus = {
    score: 0,
    lives: 3
  };

  const bricks: Brick[][] = build_bricks_conv();

  const draw = () => {
    drawBall(ctx, canvas.width, canvas.height, ball);
    drawBricks(ctx, bricks);
    drawPaddle(ctx, paddle, canvas.height);
    drawScore(ctx, status.score);
    drawLives(ctx, status.lives, canvas.width);
    collisionDetection(bricks, ball, status);
    update(canvas.width, canvas.height, ball, status, paddle);

    requestAnimationFrame(draw);
  };

  const keyDownHandler = (e: KeyboardEvent) => {
    if (e.key === "Right" || e.key === "ArrowRight") {
      paddle.rightPressed = true;
    } else if (e.key === "Left" || e.key === "ArrowLeft") {
      paddle.leftPressed = true;
    }
  };

  const keyUpHandler = (e: KeyboardEvent) => {
    if (e.key === "Right" || e.key === "ArrowRight") {
      paddle.rightPressed = false;
    } else if (e.key === "Left" || e.key === "ArrowLeft") {
      paddle.leftPressed = false;
    }
  };

  const mouseMoveHandler = (e: MouseEvent) => {
    const relativeX = e.clientX - canvas.offsetLeft;
    if (relativeX > 0 && relativeX < canvas.width) {
      paddle.x = relativeX - paddle_width() / 2;
    }
  };

  document.addEventListener("keydown", keyDownHandler, false);
  document.addEventListener("keyup", keyUpHandler, false);
  document.addEventListener("mousemove", mouseMoveHandler, false);
  draw();
};
