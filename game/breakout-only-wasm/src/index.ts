(async function() {
  const query = new URLSearchParams(new URL(location.href).search);
  const wasm = await import("tora-breakout-wasm");

  if (query.get("watch")) {
    wasm.start(ASSET_URL, true);
    return;
  }

  wasm.start(ASSET_URL, false);
})();
