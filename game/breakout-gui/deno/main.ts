import { SizeHint, Webview } from "https://deno.land/x/webview@0.7.5/mod.ts";
import { serve } from "https://deno.land/std@0.178.0/http/mod.ts";
import { serveDir } from "https://deno.land/std@0.178.0/http/file_server.ts";

export function add(a: number, b: number): number {
  return a + b;
}

const html = Deno.readTextFileSync("dist/index.html");

const webview = new Webview(true, {
  width: 600,
  height: 850,
  hint: SizeHint.FIXED,
});

webview.navigate(`data:text/html,${encodeURIComponent(html)}`);
const worker = new Worker(new URL("./img_server.ts", import.meta.url).href, { type: "module" });

webview.run();
worker.terminate();

