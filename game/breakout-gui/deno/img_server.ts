import { serve } from "https://deno.land/std@0.178.0/http/mod.ts";
import { serveDir } from "https://deno.land/std@0.178.0/http/file_server.ts";

serve((request) => serveDir(request, { enableCors: true }));

