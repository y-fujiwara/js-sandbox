import { Moment } from "moment";

export interface User {
  id: number;
  name: string;
}

export interface ProjectTask {
  name: string;
  assign: User;
  start: Moment;
  end: Moment;
  progress: number;
}

export interface MileStone {
  day: Moment;
  desc: string;
}
