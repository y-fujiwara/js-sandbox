import moment from "moment";

export default interface Project {
  id?: number;
  name: string;
  start: string;
  end: string;
  themeColor?: string;
};
export type Projects = {[id: string]: Project};
