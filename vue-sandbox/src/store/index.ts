import Vue from "vue";
import Vuex, { Store } from "vuex";
import moment, { Moment } from "moment";
import "moment/locale/ja";
import VueCompositionApi from "@vue/composition-api";
Vue.use(VueCompositionApi);

import { provide, inject } from "@vue/composition-api";
import Project, { Projects } from "@/interfaces/project_interface";
import { apiGetProjects, apiPostProject } from "@/apis/project";

Vue.use(Vuex);

/**
 * TODO: 面倒くさいのでサーバー側でstartとendはタスクの最大、最小を加味した値にすること
 */
export interface State {
  projects: Projects;
}

export default new Vuex.Store<State>({
  state: {
    projects: {}
  },

  getters: {
    projects: state => state.projects
  },
  mutations: {
    setProjects(state, projects: Projects) {
      state.projects = projects;
    }
  },
  actions: {
    async updateProject({ commit }, project: Project): Promise<Project> {
      // TODO: 本来であればプロジェクト全部が帰ってくるのが正しい 更新対象のみ帰る場合はリストの方をアップデートが必要
      return apiPostProject(project);
    },
    async getProjects({ commit }) {
      const projects = await apiGetProjects();
      commit("setProjects", projects);
    }
  },
  modules: {}
});

const StoreSymbol = "Store";

export function provideStore(store: Store<State>) {
  provide(StoreSymbol, store);
}

export function useStore(): Store<State> {
  const store = inject<Store<State>>(StoreSymbol);
  if (!store) {
    throw Error("store is not defined");
  }
  return store;
}
