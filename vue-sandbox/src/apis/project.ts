import Project, { Projects } from "@/interfaces/project_interface";

export const apiGetProjects = async (): Promise<Projects> => {
  return new Promise(resolve =>
    resolve({
      1: {
        id: 1,
        name: "Test1プロジェクト",
        start: "2020-02-01",
        end: "2020-02-29",
        themeColor: "#FB8C00"
      },
      2: {
        id: 2,
        name: "Test2プロジェクト",
        start: "2020-01-01",
        end: "2020-03-30",
        themeColor: "#6D4C41"
      }
    })
  );
};

export const apiPostProject = async (project: Project): Promise<Project> => {
  return new Promise(resolve => resolve(project));
};
