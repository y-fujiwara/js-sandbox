// graphqlテスト
import { useQuery, UseQueryReturn } from "@vue/apollo-composable";
import gql from "graphql-tag";
import { AllPhotos } from "@/graphql/types/AllPhotos";

export const allPhotos = () =>
  useQuery<AllPhotos>(gql`
    query AllPhotos {
      allPhotos {
        id
        description
        url
      }
    }
  `);
