/* tslint:disable */
/* eslint-disable */
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL query operation: AllPhotos
// ====================================================

export interface AllPhotos_allPhotos {
  __typename: "Photo";
  id: string;
  description: string;
  url: string;
}

export interface AllPhotos {
  allPhotos: AllPhotos_allPhotos[];
}
