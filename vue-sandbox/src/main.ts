import Vue from "vue";
import vuetify from "./plugins/vuetify";
import App from "./App.vue";
import router from "./router";
Vue.config.productionTip = false;
import "reset-css";
import store, { provideStore } from "@/store/index.ts";
import "material-design-icons-iconfont/dist/material-design-icons.css";
import "@mdi/font/css/materialdesignicons.css";
// @ts-ignore
import { createProvider } from "./vue-apollo.js";
import VueCompositionApi, { provide } from "@vue/composition-api";
import { DefaultApolloClient } from "@vue/apollo-composable";

Vue.use(VueCompositionApi);
const apolloProvider = createProvider();

// @ts-ignore
new Vue({
  router: router,
  store: store,
  render: h => h(App),
  vuetify,

  // @ts-ignore

  setup: function() {
    provideStore(store);
    provide(DefaultApolloClient, apolloProvider.defaultClient);
  }
}).$mount("#app");
