import {
  functionDefaultTemplateNullable,
  numberDefaultTemplate,
  objectDefaultTemplateNullable
} from "@/props/templates";

const cellProps = {
  cellWidth: { ...numberDefaultTemplate },
  date: { ...objectDefaultTemplateNullable },
  mileStones: { ...objectDefaultTemplateNullable }
};
export default cellProps;
