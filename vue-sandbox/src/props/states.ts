import { MileStone } from "@/interfaces/task_interfaces";

export declare type MileStoneState = { [key: string]: MileStone };
