import { computed } from "@vue/composition-api";
import { CELL_SIZE } from "@/consts";

export const computedCellSize = computed(() => CELL_SIZE);
export const computedTableHeaderHeight = computed(
  () => computedCellSize.value * 2
);
