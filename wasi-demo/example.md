# WebAssemblyでマークダウンライブラリを作る

この文章はサーバーサイドでWebAssemblyを利用して、マークダウンから作られたものです。

利用技術は以下になります。

- Rust
- [pulldown_cmark](https://crates.io/crates/pulldown-cmark)
- Deno
  - fresh
  - bbb
