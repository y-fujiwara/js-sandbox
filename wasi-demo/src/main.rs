use ammonia::clean;
use pulldown_cmark::{html, Options, Parser};
use std::env;
use std::fs;
use std::fs::File;
use std::io::Write;
use std::path::Path;

fn read_markdown(filename: &str) -> Result<String, String> {
    Ok(fs::read_to_string(filename)
        .map_err(|err| format!("error opening input {}: {}", filename, err))?)
}

fn parse_markdown(markdown_input: &str) -> String {
    let mut options = Options::empty();
    options.insert(Options::ENABLE_STRIKETHROUGH);
    let parser = Parser::new_ext(markdown_input, options);

    let mut html_output = String::new();
    html::push_html(&mut html_output, parser);
    clean(&*html_output)
}

fn write_html(html: &str) -> Result<(), String> {
    let path = Path::new("./example.html");

    let mut file =
        File::create(&path).map_err(|err| format!("error create file example.html: {}", err))?;
    file.write_all(html.as_bytes())
        .map_err(|err| format!("error write file example.html: {}", err))?;
    Ok(())
}

fn main() {
    let args: Vec<String> = env::args().collect();
    let program = args[0].clone();

    if args.len() < 2 {
        eprintln!("usage: {} <filepath>", program);
        return;
    }

    let contents = match read_markdown(&args[1]) {
        Ok(c) => c,
        Err(err) => {
            eprintln!("{}", err);
            return;
        }
    };

    let html = parse_markdown(&contents);
    match write_html(&html) {
        Ok(_) => println!("success markdown to html. look at example.html"),
        Err(err) => {
            eprintln!("{}", err);
        }
    };
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn parse_test() {
        let html_output =
            parse_markdown("Hello world, this is a ~~complicated~~ *very simple* example.");

        let expected_html =
            "<p>Hello world, this is a <del>complicated</del> <em>very simple</em> example.</p>\n";
        assert_eq!(expected_html, &html_output);
    }

    #[test]
    fn parse_xss_test() {
        let html_output = parse_markdown("XSS<script>alert(1);</script>");

        let expected_html = "<p>XSS</p>\n";
        assert_eq!(expected_html, &html_output);
    }
}
