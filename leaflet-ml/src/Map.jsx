import React from 'react';
import { Map, Marker, Popup, TileLayer, LayersControl } from 'react-leaflet';

const position = [35.8183556, 139.8269896];

const layerControl = (
  <LayersControl position="topright">
    <LayersControl.BaseLayer name="OpenStreetMap.BlackAndWhite">
      <TileLayer
        attribution='&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
        url="https://tiles.wmflabs.org/bw-mapnik/{z}/{x}/{y}.png"
      />
    </LayersControl.BaseLayer>
    <LayersControl.BaseLayer name="OpenStreetMap.Mapnik">
      <TileLayer
        attribution='&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
        url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
      />
    </LayersControl.BaseLayer>
    <LayersControl.BaseLayer checked={true} name="GSI Japan Map">
      <TileLayer
        attribution="<a href='https://maps.gsi.go.jp/development/ichiran.html' target='_blank'>地理院タイル</a>"
        url="https://cyberjapandata.gsi.go.jp/xyz/std/{z}/{x}/{y}.png"
      />
    </LayersControl.BaseLayer>
  </LayersControl>
);

class map extends React.Component {
  render() {
    return (
      <Map center={position} zoom={13}>
        {layerControl}
        <Marker position={position}>
          <Popup>
            A pretty CSS3 popup.
            <br />
            Easily customizable.
          </Popup>
        </Marker>
      </Map>
    );
  }
}

module.exports = map;
