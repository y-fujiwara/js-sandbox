[@bs.module] external map: ReasonReact.reactClass = "./Map.jsx";

let component = ReasonReact.statelessComponent("Map");
let make = children =>
  ReasonReact.wrapJsForReason(
    ~reactClass=map,
    ~props=Js.Obj.empty(),
    children,
  );