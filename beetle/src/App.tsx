import * as React from 'react';
import './App.css';

import Beetle from './Beetle/Beetle';

const a = 'aaa';

interface IAppState {
  beetlePoss: Array<{left: number; top: number}>;
}

class App extends React.Component<{}, IAppState> {
  constructor(props: {}) {
    super(props);
    this.state = {beetlePoss: []};
  }

  public onClick($event: React.MouseEvent) {
    this.state.beetlePoss.push({
      left: $event.pageX - 200,
      top: $event.pageY - 300,
    });

    this.setState({beetlePoss: this.state.beetlePoss});
  }

  render() {
    const clickEvent = this.onClick.bind(this);
    return (
      <div onClick={clickEvent} className="App">
        <span>please click anywhere!!</span>
        <br />
        <span>
          The beetle was referred to this{' '}
          <a href="https://qox.jp/blog/css-beetle/">site</a>. Thank you.
        </span>
        {this.state.beetlePoss.map((pos, idx) => (
          <Beetle key={idx} position={pos} />
        ))}
      </div>
    );
  }
}

export default App;
