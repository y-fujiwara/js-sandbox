import * as React from 'react';
import './Beetle.css';

interface IBeetleProps {
  position: { left: number; top: number };
}

export default class Beetle extends React.Component<IBeetleProps, {}> {
  public render(): JSX.Element {
    return (
      <div style={this.props.position} className="beetle_wrapper">
        <div className="beetle">
          <div className="foot1_3" />
          <div className="foot1_2" />
          <div className="foot1_1" />
          <div className="foot2_1" />
          <div className="foot2_2" />
          <div className="foot2_3" />
          <div className="foot3_1" />
          <div className="foot3_2" />
          <div className="foot3_3" />
          <div className="body_base" />
          <div className="neck" />
          <div className="horn" />
          <div className="horn_bottom" />
          <div className="horn_top" />
          <div className="horn2" />
          <div className="eye" />
          <div className="head" />
          <div className="head_bottom" />
          <div className="head_light" />
          <div className="horn3" />
          <div className="horn3_top" />
          <div className="body" />
          <div className="body_light" />
          <div className="body_light2_left" />
        </div>
        <div className="beetle right">
          <div className="foot1_3" />
          <div className="foot1_2" />
          <div className="foot1_1" />
          <div className="foot2_1" />
          <div className="foot2_2" />
          <div className="foot2_3" />
          <div className="foot3_1" />
          <div className="foot3_2" />
          <div className="foot3_3" />
          <div className="body_base" />
          <div className="neck" />
          <div className="horn" />
          <div className="horn_bottom" />
          <div className="horn_top" />
          <div className="horn2" />
          <div className="eye" />
          <div className="head" />
          <div className="head_bottom" />
          <div className="head_light" />
          <div className="horn3" />
          <div className="horn3_top" />
          <div className="body" />
          <div className="body_light" />
          <div className="body_light2_right" />
        </div>
      </div>
    );
  }
}
