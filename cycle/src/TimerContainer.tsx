import { Sources, Sinks } from "./interfaces";
import { CountdownTimer } from "./CountdownTimer";
import xs from 'xstream';

export function TimerContainer(sources: Sources): Sinks {
  const countdownTimer = CountdownTimer({
    props: {
      maxTime$: xs.of(5000),
      alertTime: 3000,
      active$: xs.merge(
        sources.DOM.select('#timer-gauge-play').events('click').mapTo(true),
        sources.DOM.select('#timer-gauge-pause').events('click').mapTo(false)
      ),
      reset$: sources.DOM.select('#timer-gauge-reset').events('click').mapTo(undefined)
    }
  });

  return {
    DOM: xs.combine(countdownTimer.DOM, countdownTimer.timeSpent$, countdownTimer.isActive$)
      .map(([timerGaugeDOM, timeSpent, isActive]) => {
        return <div className='container'>
          <h2 className='page-header'>Countdown Timer</h2>
          <div className='row'>
            <div className='col-sm-6'>
              {timerGaugeDOM}
              <hr />
              <div className='btn-group'>
                <button id='timer-gauge-play' className='btn btn-default'>Play</button>
                <button id='timer-gauge-pause' className='btn btn-default'>Pause</button>
                <button id='timer-gauge-reset' className='btn btn-default'>Reset</button>
              </div>
            </div>
            <div className='col-sm-6'>
              <pre>
                <code>
                  {JSON.stringify({
                    isActive: isActive,
                    result: timeSpent
                  }, null, 2)}
                </code>
              </pre>
            </div>
          </div>
        </div>;})
  };
}
