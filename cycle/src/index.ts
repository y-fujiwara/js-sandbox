import {run} from '@cycle/run';
import {makeDOMDriver} from '@cycle/dom';
import {Component} from './interfaces';
import {makeHTTPDriver} from '@cycle/http';


import {App} from './app';
import { makeScrollDriver } from './ScrollDriver';

const main : Component = App;

// tsくんが怒るので一旦any型
const drivers: any = {
  DOM: makeDOMDriver('#root'),
  Scroll: makeScrollDriver({element: document.body, furation: 600}),
  HTTP: makeHTTPDriver()
};

run(main, drivers);
