import xs from 'xstream';
import {Sources, MainSinks, PageState} from './interfaces';
import isolate from '@cycle/isolate';
import { Bmi } from './Bmi';
import { ModalContainer } from './modalCOntainer';
import { TimerContainer } from './TimerContainer';
import {Scroll} from './Scroll';

export function App(sources : Sources) : MainSinks {
  // キー入力イベント(Intent)
  const input$: xs<Event> = sources.DOM.select('.field').events('input');

  // model
  const name$: xs<string> = xs.from(input$)
    .map((ev: Event) => (ev.target as HTMLInputElement).value)
    .startWith('');

  const bmiFn = isolate(Bmi, 'bmi') as typeof Bmi;
  const modalFn = isolate(ModalContainer, 'modal') as typeof ModalContainer;
  const timerFn = isolate(TimerContainer, 'timer') as typeof TimerContainer;
  const scrollFn = isolate(Scroll, 'scroll') as typeof Scroll;

  const bmi$ = bmiFn(sources);
  const modal$ = modalFn(sources);
  const timer$ = timerFn(sources);
  const scroll$ = scrollFn(sources);

  const defaultPageState = {
    response: {}
  };

  const eventClickGet$ = sources.DOM.select('#get-posts').events('click');

  // リクエストObservableを作成
  const request$ = xs.from(eventClickGet$).mapTo({
    url: 'http://jsonplaceholder.typicode.com/posts',
    category: 'api'
  });

  // レスポンスのObservableを作成
  const response$ = (sources.HTTP.select('api') as any).flatten();

  // レスポンスから値を取る
  const pageState$ =  response$.map((response: PageState) => ({response})).startWith(defaultPageState);




  // view
  const vtree$ = xs.combine(bmi$.DOM, modal$.DOM, timer$.DOM, scroll$.DOM, name$).map(
    ([bmi, modal, timer, scroll, name]) => {
      return <div className='well'>
        <div className='form-group'>
          <label>Name: </label>
          <input className='field form-control' type='text' />
        </div>
        <hr />
        <h1>Hello {name}</h1>
        {bmi}
        {modal}
        {timer}
        {scroll}
      </div>;
  });

  return {
    DOM: vtree$,
    Scroll: scroll$.Scroll
  };
}
