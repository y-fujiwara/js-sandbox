import {ModalSources, Sinks} from './interfaces';
import {VNode} from '@cycle/dom';
import xs from 'xstream';

function View(visibility: Boolean, content: VNode): VNode {
  const classes =  visibility ? 'modal modal--visible' : 'modal';
  return <div className={classes}>
    <div className='modal__content'>
      {visibility ? content : null}
    </div>
  </div>;
}

export function Modal({props}: ModalSources): Sinks {
  const vdom$ = xs.combine(props.visibility$.startWith(false), props.content$)
    .map(([visibility, content]) => View(visibility, content));

  return {
    DOM: vdom$
  };
}
