import xs from 'xstream';
import {DOMSource, VNode} from '@cycle/dom';
import { HTTPSource } from '@cycle/http';

export type Sources = {
  DOM : DOMSource;
  Scroll: xs<string>;
  HTTP: HTTPSource
};

export type SliderSources = {
  DOM : DOMSource;
  props: xs<SliderProps>;
};

export type ModalSources = {
  props: ModalProps;
};

export type TimerSources = {
  props: TimerProps;
};

export type Sinks = {
  DOM : xs<VNode>;
};

export type MainSinks = {
  DOM : xs<VNode>;
  Scroll: xs<number>;
}

export type SliderSinks = {
  DOM : xs<VNode>;
  value$: xs<number>;
};

export type TimerSinks = {
  DOM: xs<VNode>;
  isActive$: xs<boolean>;
  timeSpent$: xs<number>;
};

export type ScrollSinks = {
  DOM: xs<VNode>;
  Scroll: xs<number>;
}

export type Component = (s : Sources) => Sinks;

export type SliderProps = {
  label: string;
  unit: string;
  min: number;
  max: number;
  init: number;
};

export type ModalProps = {
  content$: xs<VNode>;
  visibility$: xs<boolean>;
};

export type TimerProps = {
  maxTime$: xs<number>;
  alertTime: number;
  active$: xs<boolean>;
  reset$: xs<undefined>;
};

export type PageState = {
  response: Object;
};
 