import { Sources, ScrollSinks } from "./interfaces";
import xs from 'xstream';
import { VNode } from "../node_modules/@cycle/dom";

function Intent(sources: Sources): xs<Event> {
  return sources.DOM.select('.scrollable__input').events('input');
}

function Model(input: xs<Event>): xs<number> {
  return input.map((ev: Event): number =>
    Number((ev.currentTarget as HTMLInputElement).value));
}

function View(model: xs<number>, scroll$: xs<string>): xs<VNode> {
  return xs.combine(scroll$.startWith('0px'), model.startWith(0)).map(
    ([scroll, offsetTop]) => {
      return <div className='scrollable'>
        <input className='scrollable__input form-control' type='number' value={offsetTop} />
        <p className='scrollable__counter'>{scroll}</p>
      </div>;
    }
  );
}

export function Scroll(sources: Sources): ScrollSinks {
  const input$ = Intent(sources);
  const offsetTop$ = Model(input$);
  const vdom$ = View(offsetTop$, sources.Scroll);
  
  return {
    DOM: vdom$,
    Scroll: offsetTop$
  };
}