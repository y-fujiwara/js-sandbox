import { Sources } from "./interfaces";
import xs from 'xstream';
import { SliderProps } from './interfaces';
import LabelSlider from './LabelSlider';
import isolate from '@cycle/isolate';

export function Bmi(sources: Sources) {
  const widthSliderProps$ = xs.of<SliderProps>({
    label: 'Weight',
    unit: 'kg',
    min: 40,
    max: 150,
    init: 60
  });

  const heightSliderProps$ = xs.of<SliderProps>({
    label: 'Height',
    unit: 'cm',
    min: 120,
    max: 220,
    init: 150
  });

  const weightSlider = isolate(LabelSlider, 'weight') as typeof LabelSlider;
  const heightSlider = isolate(LabelSlider, 'height') as typeof LabelSlider;;

  const widthLabelSlider$ = weightSlider({DOM: sources.DOM, props: widthSliderProps$});
  const heightLabelSlider$ = heightSlider({DOM: sources.DOM, props: heightSliderProps$});

  // model
  const bmi$ = xs.combine(widthLabelSlider$.value$, heightLabelSlider$.value$).map(([weight, height]) => {
    const heightMeters = height * 0.01;
    const bmi = Math.round(weight / (heightMeters * heightMeters));
    return bmi;
  }).remember();

  // view
  const vtree$ = xs.combine(bmi$, widthLabelSlider$.DOM, heightLabelSlider$.DOM).map(
    ([bmi, wSlider, hSlider]) => {
        return <div>
          {wSlider}
          {hSlider}
          <h1>{bmi}</h1>
        </div>;
    });

  return {
    DOM: vtree$
  };
}
