export default class TimerState {
  constructor(public isPlaying = false, public timeSpent = 0, private lastTime = -1) { }

  public tick(): this {
    const now = Date.now();

    if (this.lastTime > 0) {
      this.timeSpent += (now - this.lastTime);
    }

    this.lastTime = now;
    return this;
  }

  public setPlaying(playing: boolean): this {
    this.isPlaying = playing;
    if (!this.isPlaying) {
      this.lastTime = -1;
    }
    return this;
  }

  public reset(): this {
    this.isPlaying = false;
    this.timeSpent = 0;
    this.lastTime = -1;
    return this;
  }

}