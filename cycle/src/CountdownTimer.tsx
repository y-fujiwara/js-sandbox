import xs from 'xstream';
import dropRepeats from 'xstream/extra/dropRepeats'
import { TimerSources, TimerSinks } from './interfaces';
import TimerState from './TimerState';
import { VNode } from '@cycle/dom';

const digit = (n: number): string => {
  return (`0${n}`).substr(-2);
}

function View({maxTime, time, alertTime}: {alertTime: number, maxTime: number, time: number}): VNode {
  const min = digit(Math.floor(time / 60000));
  const sec = digit(Math.floor(time / 1000) % 60);
  const msec = digit(Math.floor(Math.floor(time / 10)));
  const ratio = time / maxTime;
  const isAlert = time < alertTime;
  const progressClass = isAlert ? 'progress-bar progress-bar-danger' : 'progress-bar';
  const timerClass = isAlert ? 'countdown-timer__counter countdown-timer__counter--danger' 
    : 'countdown-timer__counter';

  return <div className='countdown-timer'>
    <div className='countdown-timer__col countdown-timer__col--progress'>
      <div className='progress'>
        <div className={progressClass} style={{width: `${ratio * 100}%`}} />
      </div>
    </div>
    <div className='countdown-timer__col'>
      <span className={timerClass}>
        {min}:{sec}:{msec}
      </span>
    </div>
  </div>;
}

export function CountdownTimer({props}: TimerSources): TimerSinks {
  const active$ = props.active$;
  // mergeは引数のいずれかにデータが来るとその後も処理される
  const model$ = xs.merge(
    // stateの各メソッドはthisを返すのでstartWith以降の引数もstateで良くなる
    // switchMapはこれでいいらしい switchMap本来の挙動は処理中に次のstreamが来たら前のはキャンセルされる者
    xs.merge(
      active$,
      props.reset$ ? props.reset$.mapTo(false) : xs.never()
    ).map((active: boolean) => active ? xs.periodic(33) : xs.of(0)).flatten()
     .map(() => (acc: TimerState) => acc.tick()),
    active$.map((playing: boolean) => (acc: TimerState) => acc.setPlaying(playing)),
    props.reset$ ? props.reset$.map((_) => (acc: TimerState) => acc.reset()) : xs.never()
  ).startWith(
    (seed: TimerState) => seed
  ).fold(
    // 第二引数は初期値
    // 第一引数の第二引数に値が流れる
    // 第一引数の第一引数が処理中の結果
    (acc: TimerState, fn: (acc: TimerState) => TimerState) => fn(acc), new TimerState()
  );

  const vdom$ = xs.combine(model$, props.maxTime$).map(([state, maxTime]) => {
    const remainTime = maxTime - state.timeSpent;
    return {
      time: remainTime < 0 ? 0 : remainTime,
      maxTime
    };
  }).compose(dropRepeats()).map( // dropRepeatsは直前のものと同じ場合に弾いてくれる
    ({time, maxTime}): VNode => View({alertTime: props.alertTime, maxTime, time})
  );

  return {
    isActive$: model$.map((state: TimerState): boolean => state.isPlaying),
    timeSpent$: model$.map((state: TimerState): number => state.timeSpent / 1000),
    DOM: vdom$
  };
}