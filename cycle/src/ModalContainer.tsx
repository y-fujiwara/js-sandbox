import {Modal} from './Modal';
import xs from 'xstream';
import {Sources, Sinks} from './interfaces';

export function ModalContainer(sources: Sources): Sinks {
  const modal = Modal({
    props: {
      // モーダルの前面に表示する DOM 要素
      content$: xs.of(
        <div className='container'>
          <div className='row' >
            <div className='col-sm-6 col-sm-offset-3'>
              <div className='panel panel-default'>
                <header className='panel-heading'>
                  <button id='dialog-close' className='close'>
                    <span>x</span>
                  </button>
                  <h4 className='panel-title'>Dummy text</h4>
                </header>
                <div className='panel-body'>
                  <p>Vivamus suscipit ...</p>
                </div>
              </div>
            </div>
          </div>
        </div>
      ),
      // モーダルを表示するかどうか
      visibility$: xs.merge(
        sources.DOM.select('#dialog-open').events('click').mapTo(true),
        sources.DOM.select('#dialog-close').events('click').mapTo(false)
      ).startWith(false)
    }
  });

  return {
    DOM: xs.from(modal.DOM).map((modalDOM) => {
      return <div className='container'>
        <h2 className='page-header'>Modal</h2>
        <div>
          <button id='dialog-open' className='btn btn-default'>Open</button>
          {modalDOM}
        </div>
      </div>
    })
  };
}
