import xs from 'xstream';
import {SliderSources, SliderSinks, SliderProps} from './interfaces';
import { VNode, DOMSource } from '@cycle/dom';

function intent(DOM: DOMSource): xs<number> {
  return DOM.select('.slider')
    .events('input')
    .map((ev: Event) => parseInt((ev.target as HTMLInputElement).value));
}

function model(newValue$: xs<number>, props$: xs<SliderProps>): xs<number> {
  console.log(props$);
  const initialValue = props$.map(props => props.init).take(1);
  return xs.merge(initialValue, newValue$).remember();
}

function view(state$: xs<[SliderProps, number]>): xs<VNode> {
  return state$.map(([props, value]) => {
    return <div>
      <label>{props.label}: {value}{props.unit}</label>
      <input className='slider form-control' type='range' 
        min={props.min} max={props.max} value={value} />
    </div>;
  });
}

export default function LabelSlider(souces: SliderSources): SliderSinks {
  const change$ = intent(souces.DOM);
  const state$ = model(change$, souces.props);
  const vdom$ = view(xs.combine(souces.props, state$));

  return {
    DOM: vdom$,
    value$: state$
  };
}