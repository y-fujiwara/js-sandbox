import xs, { Subscription } from 'xstream';
import { source } from '../node_modules/@cycle/dom';

export function makeScrollDriver(options: any) {
  const {element, duration}: {element: HTMLElement, duration: number} = options;

  const ScrollDriver = (sink$: xs<any>): xs<{}> => {
    // Rx.Subjectの代わり
    const source$ = xs.create();

    window.addEventListener('scroll', () => {
      // nextの代わりはこれ
      source$.shamefullySendNext(`${window.scrollY}px`);
    })

    sink$.subscribe({
      next: (offsetTop: number) => {
        console.log(offsetTop);
        return window.scrollTo(0, offsetTop)
      },
      error: () => {},
      complete: () => {}
    });

    return source$;
  };

  return ScrollDriver;
}